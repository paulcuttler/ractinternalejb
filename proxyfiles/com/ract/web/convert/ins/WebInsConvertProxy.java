/*
**
**    Created by PROGRESS ProxyGen (Progress Version 10.1C) Fri Jun 24 16:14:40 EST 2011
**
*/

package com.ract.web.convert.ins;

import com.progress.open4gl.*;
import com.progress.common.ehnlog.IAppLogger;
import com.progress.common.ehnlog.LogUtils;
import com.progress.open4gl.dynamicapi.IPoolProps;
import com.progress.open4gl.javaproxy.Connection;
import com.progress.message.jcMsg;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.ResultSet;
import java.io.IOException;

//
// WebInsConvertProxy
//
/**
*    
*
*    @author    dgk
*    @version 18-12-2008
*/
public class WebInsConvertProxy implements SDOFactory
{
    // "This proxy version is not compatible with the current
    // version of the dynamic API."
    protected static final long m_wrongProxyVer = jcMsg.jcMSG079;

    private   static final int  PROXY_VER = 5;

    protected WebInsConvertProxyImpl m_WebInsConvertProxyImpl;

    //---- Constructors
    public WebInsConvertProxy(Connection connection)
        throws Open4GLException,
               ConnectException,
               SystemErrorException,
               IOException
    {
        /* we must do this here before we attempt to create the appobject */
        if (RunTimeProperties.getDynamicApiVersion() != PROXY_VER)
            throw new Open4GLException(m_wrongProxyVer, null);

        String urlString = connection.getUrl();
        if (urlString == null || urlString.compareTo("") == 0)
            connection.setUrl("com.ract.web.convert.ins.WebInsConvertProxy");

        m_WebInsConvertProxyImpl = new WebInsConvertProxyImpl(
                                  "com.ract.web.convert.ins.WebInsConvertProxy",
                                  connection,
                                  RunTimeProperties.tracer);
    }

    public WebInsConvertProxy(String urlString,
                        String userId,
                        String password,
                        String appServerInfo)
        throws Open4GLException,
               ConnectException,
               SystemErrorException,
               IOException
    {
        Connection connection;

        /* we must do this here before we attempt to create the appobject */
        if (RunTimeProperties.getDynamicApiVersion() != PROXY_VER)
            throw new Open4GLException(m_wrongProxyVer, null);

        connection = new Connection(urlString,
                                    userId,
                                    password,
                                    appServerInfo);

        m_WebInsConvertProxyImpl = new WebInsConvertProxyImpl(
                                  "com.ract.web.convert.ins.WebInsConvertProxy",
                                  connection,
                                  RunTimeProperties.tracer);

        /* release the connection since the connection object */
        /* is being destroyed.  the user can't do this        */
        connection.releaseConnection();
    }

    public WebInsConvertProxy(String userId,
                        String password,
                        String appServerInfo)
        throws Open4GLException,
               ConnectException,
               SystemErrorException,
               IOException
    {
        Connection connection;

        /* we must do this here before we attempt to create the appobject */
        if (RunTimeProperties.getDynamicApiVersion() != PROXY_VER)
            throw new Open4GLException(m_wrongProxyVer, null);

        connection = new Connection("com.ract.web.convert.ins.WebInsConvertProxy",
                                    userId,
                                    password,
                                    appServerInfo);

        m_WebInsConvertProxyImpl = new WebInsConvertProxyImpl(
                                  "com.ract.web.convert.ins.WebInsConvertProxy",
                                  connection,
                                  RunTimeProperties.tracer);

        /* release the connection since the connection object */
        /* is being destroyed.  the user can't do this        */
        connection.releaseConnection();
    }

    public WebInsConvertProxy()
        throws Open4GLException,
               ConnectException,
               SystemErrorException,
               IOException
    {
        Connection connection;

        /* we must do this here before we attempt to create the appobject */
        if (RunTimeProperties.getDynamicApiVersion() != PROXY_VER)
            throw new Open4GLException(m_wrongProxyVer, null);

        connection = new Connection("com.ract.web.convert.ins.WebInsConvertProxy",
                                    null,
                                    null,
                                    null);

        m_WebInsConvertProxyImpl = new WebInsConvertProxyImpl(
                                  "com.ract.web.convert.ins.WebInsConvertProxy",
                                  connection,
                                  RunTimeProperties.tracer);

        /* release the connection since the connection object */
        /* is being destroyed.  the user can't do this        */
        connection.releaseConnection();
    }

    public void _release() throws Open4GLException, SystemErrorException
    {
        m_WebInsConvertProxyImpl._release();
    }

    //---- Get Connection Id
    public Object _getConnectionId() throws Open4GLException
    {
        return (m_WebInsConvertProxyImpl._getConnectionId());
    }

    //---- Get Request Id
    public Object _getRequestId() throws Open4GLException
    {
        return (m_WebInsConvertProxyImpl._getRequestId());
    }

    //---- Get SSL Subject Name
    public Object _getSSLSubjectName() throws Open4GLException
    {
        return (m_WebInsConvertProxyImpl._getSSLSubjectName());
    }

    //---- Is there an open output temp-table?
    public boolean _isStreaming() throws Open4GLException
    {
        return (m_WebInsConvertProxyImpl._isStreaming());
    }

    //---- Stop any outstanding request from any object that shares this connection.
    public void _cancelAllRequests() throws Open4GLException
    {
        m_WebInsConvertProxyImpl._cancelAllRequests();
    }

    //---- Return the last Return-Value from a Progress procedure
    public String _getProcReturnString() throws Open4GLException
    {
        return (m_WebInsConvertProxyImpl._getProcReturnString());
    }

    //---- Create an SDO ResultSet object - There are 3 overloaded variations
    public SDOResultSet _createSDOResultSet(String procName)
        throws Open4GLException, ProSQLException
    {
        return (m_WebInsConvertProxyImpl._createSDOResultSet(procName, null, null, null));
    }

    public SDOResultSet _createSDOResultSet(String procName,
                                            String whereClause,String sortBy)
        throws Open4GLException, ProSQLException
    {
        return (m_WebInsConvertProxyImpl._createSDOResultSet(procName, whereClause, sortBy, null));
    }

    public SDOResultSet _createSDOResultSet(String procName,
                                          String whereClause,
                                          String sortBy,
                                          SDOParameters params)
        throws Open4GLException, ProSQLException
    {
        return (m_WebInsConvertProxyImpl._createSDOResultSet(procName, whereClause, sortBy, params));
    }

    // Create the ProcObject that knows how to talk to SDO's.
    public SDOInterface _createSDOProcObject(String procName)
        throws Open4GLException
    {
        return (m_WebInsConvertProxyImpl._createSDOProcObject(procName));
    }

	/**
	*	
	*	
	*	Schema of input result set; Parameter 5
	*		Field:fieldName character (java.lang.String)
	*		Field:fieldValue character (java.lang.String)
	*	Schema of input result set; Parameter 6
	*		Field:webClientNo integer (java.lang.Integer)
	*		Field:ractClientNo integer (java.lang.Integer)
	*		Field:isDriver logical (java.lang.Boolean)
	*		Field:isOwner logical (java.lang.Boolean)
	*		Field:isGroup logical (java.lang.Boolean)
	*		Field:isPreferredAddress logical (java.lang.Boolean)
	*		Field:drivingFrequency character (java.lang.String)
	*		Field:yearCommencedDriving integer (java.lang.Integer)
	*		Field:numberOfAccidents integer (java.lang.Integer)
	*		Field:currentNcd integer (java.lang.Integer)
	*		Field:givenNames character (java.lang.String)
	*		Field:surname character (java.lang.String)
	*		Field:gender character (java.lang.String)
	*		Field:dateOfBirth date (java.sql.Date)
	*	Schema of input result set; Parameter 7
	*		Field:webClientNo integer (java.lang.Integer)
	*		Field:detType character (java.lang.String)
	*		Field:detMonth integer (java.lang.Integer)
	*		Field:detYear integer (java.lang.Integer)
	*		Field:detail character (java.lang.String)
	*/
	public void convertWebPolicy(int webQuoteNo, String coverType, String quoteStatus, java.util.GregorianCalendar coverDate, java.sql.ResultSet tQuoteDetail, java.sql.ResultSet tClient, java.sql.ResultSet tClientDet, String pUserId, StringHolder pResult, IntHolder pPolicyNo)
		throws Open4GLException, RunTime4GLException, SystemErrorException
	{
		m_WebInsConvertProxyImpl.convertWebPolicy( webQuoteNo,  coverType,  quoteStatus,  coverDate,  tQuoteDetail,  tClient,  tClientDet,  pUserId,  pResult,  pPolicyNo);
	}

	/**
	*	
	*	
	*	Schema of input result set; Parameter 6
	*		Field:fieldName character (java.lang.String)
	*		Field:fieldValue character (java.lang.String)
	*		Field:stringKey character (java.lang.String)
	*		Field:intKey integer (java.lang.Integer)
	*	Schema of input result set; Parameter 7
	*		Field:siSeqNo integer (java.lang.Integer)
	*		Field:siClass character (java.lang.String)
	*	Schema of input result set; Parameter 8
	*		Field:siSeqNo integer (java.lang.Integer)
	*		Field:siLineNo integer (java.lang.Integer)
	*		Field:siDesc character (java.lang.String)
	*		Field:siSerialNo character (java.lang.String)
	*		Field:siSumIns integer (java.lang.Integer)
	*	Schema of input result set; Parameter 9
	*		Field:webClientNo integer (java.lang.Integer)
	*		Field:ractClientNo integer (java.lang.Integer)
	*		Field:isDriver logical (java.lang.Boolean)
	*		Field:isOwner logical (java.lang.Boolean)
	*		Field:isGroup logical (java.lang.Boolean)
	*		Field:isPreferredAddress logical (java.lang.Boolean)
	*		Field:drivingFrequency character (java.lang.String)
	*		Field:yearCommencedDriving integer (java.lang.Integer)
	*		Field:numberOfAccidents integer (java.lang.Integer)
	*		Field:currentNcd integer (java.lang.Integer)
	*		Field:givenNames character (java.lang.String)
	*		Field:surname character (java.lang.String)
	*		Field:gender character (java.lang.String)
	*		Field:dateOfBirth date (java.sql.Date)
	*	Schema of input result set; Parameter 10
	*		Field:webClientNo integer (java.lang.Integer)
	*		Field:detType character (java.lang.String)
	*		Field:detMonth integer (java.lang.Integer)
	*		Field:detYear integer (java.lang.Integer)
	*		Field:detail character (java.lang.String)
	*/
	public void convertWebQuote(int webQuoteNo, String coverType, String quoteStatus, java.util.GregorianCalendar pQuoteDate, java.util.GregorianCalendar pStartDate, java.sql.ResultSet tQuoteDetail, java.sql.ResultSet tSi, java.sql.ResultSet tSiLine, java.sql.ResultSet tClient, java.sql.ResultSet tClientDet, String pUserId, StringHolder pResult, IntHolder pPolicyNo)
		throws Open4GLException, RunTime4GLException, SystemErrorException
	{
		m_WebInsConvertProxyImpl.convertWebQuote( webQuoteNo,  coverType,  quoteStatus,  pQuoteDate,  pStartDate,  tQuoteDetail,  tSi,  tSiLine,  tClient,  tClientDet,  pUserId,  pResult,  pPolicyNo);
	}

	/**
	*	
	*	
	*/
	public void getEligibleDiscount(int pClientNo, String pProdType, String pRiskType, java.util.GregorianCalendar pStartDate, BigDecimalHolder pDiscount)
		throws Open4GLException, RunTime4GLException, SystemErrorException
	{
		m_WebInsConvertProxyImpl.getEligibleDiscount( pClientNo,  pProdType,  pRiskType,  pStartDate,  pDiscount);
	}

	/**
	*	
	*	
	*/
	public void getLookupDescription(int pSeq, String pKey, String pSubKey, StringHolder pClass, BigDecimalHolder pValue, StringHolder pDesc, StringHolder pUnit, StringHolder pRiskType, StringHolder pType, StringHolder pSubType)
		throws Open4GLException, RunTime4GLException, SystemErrorException
	{
		m_WebInsConvertProxyImpl.getLookupDescription( pSeq,  pKey,  pSubKey,  pClass,  pValue,  pDesc,  pUnit,  pRiskType,  pType,  pSubType);
	}

	/**
	*	
	*	
	*/
	public void getWebInsBranches(StringHolder pBranchList)
		throws Open4GLException, RunTime4GLException, SystemErrorException
	{
		m_WebInsConvertProxyImpl.getWebInsBranches( pBranchList);
	}



}
