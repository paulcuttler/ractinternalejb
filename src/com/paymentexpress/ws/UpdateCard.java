package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="postUsername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="postPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "postUsername", "postPassword", "cardDetails" })
@XmlRootElement(name = "UpdateCard")
public class UpdateCard {

	protected String postUsername;
	protected String postPassword;
	protected String cardDetails;

	/**
	 * Gets the value of the postUsername property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostUsername() {
		return postUsername;
	}

	/**
	 * Sets the value of the postUsername property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPostUsername(String value) {
		this.postUsername = value;
	}

	/**
	 * Gets the value of the postPassword property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPostPassword() {
		return postPassword;
	}

	/**
	 * Sets the value of the postPassword property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPostPassword(String value) {
		this.postPassword = value;
	}

	/**
	 * Gets the value of the cardDetails property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardDetails() {
		return cardDetails;
	}

	/**
	 * Sets the value of the cardDetails property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardDetails(String value) {
		this.cardDetails = value;
	}

}
