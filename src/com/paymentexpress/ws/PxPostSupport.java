package com.paymentexpress.ws;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import com.ract.util.FileUtil;

/**
 * PxPostSupport provides methods to support POSTing payment transactions to the PxPost endpoint hosted by Payment Express This wholly replaces the use of PxPay web service in the Web Conversion sub system This code POSTs to a Mulesoft ESB endpoint (/pxpost) which proxys the POST to the PxPost endpoint This approach was taken to overcome the TLS issues following PEs announcement of the dropping of TLS 1.0 support on 17/4/2018
 * 
 * Mulesoft issues the POST using TLS 1.2
 * 
 * @author sharpg-admin
 * 
 */
public class PxPostSupport {

	final static public String DPS_USERNAME_KEY = "dpsUsername";
	final static public String DPS_PASSWORD_KEY = "dpsPassword";
	final static public String DPS_MULESOFT_URL_KEY = "dpsMulesoftUrl";
	final static public String EMPTY_STRING = "";
	private static String dpsUserName = EMPTY_STRING;
	private static String dpsPassword = EMPTY_STRING;
	private static String dpsMulesoftUrl = EMPTY_STRING;

	/**
	 * This static method accepts transactions details, determines the appropriate source for the security credentials and end point URL and calls the real POST method
	 * 
	 * @author sharpg-admin
	 * 
	 */
	public static String postHTTPRequest(TransactionDetailsPxPost txnDetails, String dpsUn, String dpsPwd, String dpsMUrl) throws Exception {
		if (dpsUn == null || dpsPwd == null || dpsMUrl == null || dpsUn.length() == 0 || dpsPwd.length() == 0 || dpsMUrl.length() == 0) {
			dpsUserName = FileUtil.getProperty("master", DPS_USERNAME_KEY);
			dpsPassword = FileUtil.getProperty("master", DPS_PASSWORD_KEY);
			dpsMulesoftUrl = FileUtil.getProperty("master", DPS_MULESOFT_URL_KEY);
		} else {
			dpsUserName = dpsUn;
			dpsPassword = dpsPwd;
			dpsMulesoftUrl = dpsMUrl;
		}

		return postHTTPRequest(txnDetails);
	}
	/**
	 * This static method accepts transactions details and performs the POST
	 * 
	 * @author sharpg-admin
	 * 
	 */
	private static String postHTTPRequest(TransactionDetailsPxPost txnDetails) throws Exception {

		// Mulesoft POST accept XML as a string and passes through to PxPost
		String XMLText = "<Txn>" + "<PostUsername>" + dpsUserName + "</PostUsername>" + "<PostPassword>" + dpsPassword + "</PostPassword>" + "<CardHolderName>" + txnDetails.getCardHolderName() + "</CardHolderName>" + "<CardNumber>" + txnDetails.getCardNumber() + "</CardNumber>" + "<Amount>" + txnDetails.getAmount() + "</Amount>" + "<DateExpiry>" + txnDetails.getDateExpiry() + "</DateExpiry>" + "<Cvc2>" + txnDetails.getCvc2() + "</Cvc2>" + "<Cvc2Presence>1</Cvc2Presence>" + "<InputCurrency>AUD</InputCurrency>" + "<TxnType>" + txnDetails.getTxnType() + "</TxnType>" + "<TxnId>" + txnDetails.getTxnRef() + "</TxnId>" + "<MerchantReference>" + txnDetails.getMerchantReference() + "</MerchantReference>" + "</Txn>";

		String resultXML = EMPTY_STRING;

		// Perform the POST - andy Exception is propogated to the caller
		PostMethod post = new PostMethod(dpsMulesoftUrl);
		try {
			StringRequestEntity requestEntity = new StringRequestEntity(XMLText);
			post.setRequestEntity(requestEntity);
			post.setRequestHeader("Content-type", "text/xml; charset=ISO-8859-1");
			HttpClient httpclient = new HttpClient();

			int result = httpclient.executeMethod(post);

			// If result not 200 throw an exception
			if (result != 200)
				throw new Exception("Server problem - result code is problematic (not 200): " + result);

			resultXML = post.getResponseBodyAsString();
		} finally {
			// Release the HTTP connection
			post.releaseConnection();
		}

		return resultXML;
	}
}