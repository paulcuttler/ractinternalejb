package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for riskRuleMatch complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="riskRuleMatch">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="matchedAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="matchedTransactionCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="action" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="period" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="includedTransactions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="amountLimit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionCountLimit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "riskRuleMatch", propOrder = { "matchedAmount",
		"matchedTransactionCount", "name", "type", "action", "period",
		"includedTransactions", "amountLimit", "transactionCountLimit" })
public class RiskRuleMatch {

	protected String matchedAmount;
	protected int matchedTransactionCount;
	protected String name;
	protected String type;
	protected String action;
	protected int period;
	protected String includedTransactions;
	protected String amountLimit;
	protected int transactionCountLimit;

	/**
	 * Gets the value of the matchedAmount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMatchedAmount() {
		return matchedAmount;
	}

	/**
	 * Sets the value of the matchedAmount property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMatchedAmount(String value) {
		this.matchedAmount = value;
	}

	/**
	 * Gets the value of the matchedTransactionCount property.
	 * 
	 */
	public int getMatchedTransactionCount() {
		return matchedTransactionCount;
	}

	/**
	 * Sets the value of the matchedTransactionCount property.
	 * 
	 */
	public void setMatchedTransactionCount(int value) {
		this.matchedTransactionCount = value;
	}

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the value of the name property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setName(String value) {
		this.name = value;
	}

	/**
	 * Gets the value of the type property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setType(String value) {
		this.type = value;
	}

	/**
	 * Gets the value of the action property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the value of the action property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAction(String value) {
		this.action = value;
	}

	/**
	 * Gets the value of the period property.
	 * 
	 */
	public int getPeriod() {
		return period;
	}

	/**
	 * Sets the value of the period property.
	 * 
	 */
	public void setPeriod(int value) {
		this.period = value;
	}

	/**
	 * Gets the value of the includedTransactions property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIncludedTransactions() {
		return includedTransactions;
	}

	/**
	 * Sets the value of the includedTransactions property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setIncludedTransactions(String value) {
		this.includedTransactions = value;
	}

	/**
	 * Gets the value of the amountLimit property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmountLimit() {
		return amountLimit;
	}

	/**
	 * Sets the value of the amountLimit property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAmountLimit(String value) {
		this.amountLimit = value;
	}

	/**
	 * Gets the value of the transactionCountLimit property.
	 * 
	 */
	public int getTransactionCountLimit() {
		return transactionCountLimit;
	}

	/**
	 * Sets the value of the transactionCountLimit property.
	 * 
	 */
	public void setTransactionCountLimit(int value) {
		this.transactionCountLimit = value;
	}

}
