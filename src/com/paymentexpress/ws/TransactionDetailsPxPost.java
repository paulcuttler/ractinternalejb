package com.paymentexpress.ws;

/**
 * PxPost transaction Details (mimics the old WebService container)
 * @author sharpg-admin
 *
 */
public class TransactionDetailsPxPost {
	private String amount;
	private String cardHolderName;
	private String cardNumber;
	private String cvc2;
	private String dateExpiry;
	private String merchantReference;
	private String txnRef;
	private String txnType;

	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * @return the cardHolderName
	 */
	public String getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * @param cardHolderName the cardHolderName to set
	 */
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the cvc2
	 */
	public String getCvc2() {
		return cvc2;
	}

	/**
	 * @param cvc2 the cvc2 to set
	 */
	public void setCvc2(String cvc2) {
		this.cvc2 = cvc2;
	}

	/**
	 * @return the dateExpiry
	 */
	public String getDateExpiry() {
		return dateExpiry;
	}

	/**
	 * @param expiryDate the dateExpiry to set
	 */
	public void setDateExpiry(String expiryDate) {
		this.dateExpiry = expiryDate;
	}

	/**
	 * @return the merchantReference
	 */
	public String getMerchantReference() {
		return merchantReference;
	}

	/**
	 * @param merchantReference the merchantReference to set
	 */
	public void setMerchantReference(String merchantReference) {
		this.merchantReference = merchantReference;
	}

	/**
	 * @return the txnRef
	 */
	public String getTxnRef() {
		return txnRef;
	}

	/**
	 * @param txnRef the txnRef to set
	 */
	public void setTxnRef(String txnRef) {
		this.txnRef = txnRef;
	}

	/**
	 * @return the txnType
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * @param txnType the txnType to set
	 */
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

}
