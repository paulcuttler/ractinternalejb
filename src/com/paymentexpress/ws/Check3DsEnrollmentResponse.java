package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Check3dsEnrollmentResult" type="{http://PaymentExpress.com}EnrolmentCheckResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "check3DsEnrollmentResult" })
@XmlRootElement(name = "Check3dsEnrollmentResponse")
public class Check3DsEnrollmentResponse {

	@XmlElement(name = "Check3dsEnrollmentResult")
	protected EnrolmentCheckResult check3DsEnrollmentResult;

	/**
	 * Gets the value of the check3DsEnrollmentResult property.
	 * 
	 * @return possible object is {@link EnrolmentCheckResult }
	 * 
	 */
	public EnrolmentCheckResult getCheck3DsEnrollmentResult() {
		return check3DsEnrollmentResult;
	}

	/**
	 * Sets the value of the check3DsEnrollmentResult property.
	 * 
	 * @param value
	 *          allowed object is {@link EnrolmentCheckResult }
	 * 
	 */
	public void setCheck3DsEnrollmentResult(EnrolmentCheckResult value) {
		this.check3DsEnrollmentResult = value;
	}

}
