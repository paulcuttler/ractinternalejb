package com.paymentexpress.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for ArrayOfRiskRuleMatch complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRiskRuleMatch">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="riskRuleMatch" type="{http://PaymentExpress.com}riskRuleMatch" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRiskRuleMatch", propOrder = { "riskRuleMatch" })
public class ArrayOfRiskRuleMatch {

	@XmlElement(nillable = true)
	protected List<RiskRuleMatch> riskRuleMatch;

	/**
	 * Gets the value of the riskRuleMatch property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the riskRuleMatch property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getRiskRuleMatch().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link RiskRuleMatch }
	 * 
	 * 
	 */
	public List<RiskRuleMatch> getRiskRuleMatch() {
		if (riskRuleMatch == null) {
			riskRuleMatch = new ArrayList<RiskRuleMatch>();
		}
		return this.riskRuleMatch;
	}

}
