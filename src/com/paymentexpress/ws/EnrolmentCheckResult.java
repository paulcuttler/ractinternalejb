package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for EnrolmentCheckResult complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="EnrolmentCheckResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enrolled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paReq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="acsURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnrolmentCheckResult", propOrder = { "enrolled", "paReq",
		"acsURL" })
public class EnrolmentCheckResult {

	protected String enrolled;
	protected String paReq;
	protected String acsURL;

	/**
	 * Gets the value of the enrolled property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnrolled() {
		return enrolled;
	}

	/**
	 * Sets the value of the enrolled property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setEnrolled(String value) {
		this.enrolled = value;
	}

	/**
	 * Gets the value of the paReq property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaReq() {
		return paReq;
	}

	/**
	 * Sets the value of the paReq property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaReq(String value) {
		this.paReq = value;
	}

	/**
	 * Gets the value of the acsURL property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcsURL() {
		return acsURL;
	}

	/**
	 * Sets the value of the acsURL property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAcsURL(String value) {
		this.acsURL = value;
	}

}
