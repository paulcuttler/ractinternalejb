package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubmitTransactionResult" type="{http://PaymentExpress.com}TransactionResult2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "submitTransactionResult" })
@XmlRootElement(name = "SubmitTransactionResponse")
public class SubmitTransactionResponse {

	@XmlElement(name = "SubmitTransactionResult")
	protected TransactionResult2 submitTransactionResult;

	/**
	 * Gets the value of the submitTransactionResult property.
	 * 
	 * @return possible object is {@link TransactionResult2 }
	 * 
	 */
	public TransactionResult2 getSubmitTransactionResult() {
		return submitTransactionResult;
	}

	/**
	 * Sets the value of the submitTransactionResult property.
	 * 
	 * @param value
	 *          allowed object is {@link TransactionResult2 }
	 * 
	 */
	public void setSubmitTransactionResult(TransactionResult2 value) {
		this.submitTransactionResult = value;
	}

}
