package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SubmitTransaction2Result" type="{http://PaymentExpress.com}TransactionResult2" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "submitTransaction2Result" })
@XmlRootElement(name = "SubmitTransaction2Response")
public class SubmitTransaction2Response {

	@XmlElement(name = "SubmitTransaction2Result")
	protected TransactionResult2 submitTransaction2Result;

	/**
	 * Gets the value of the submitTransaction2Result property.
	 * 
	 * @return possible object is {@link TransactionResult2 }
	 * 
	 */
	public TransactionResult2 getSubmitTransaction2Result() {
		return submitTransaction2Result;
	}

	/**
	 * Sets the value of the submitTransaction2Result property.
	 * 
	 * @param value
	 *          allowed object is {@link TransactionResult2 }
	 * 
	 */
	public void setSubmitTransaction2Result(TransactionResult2 value) {
		this.submitTransaction2Result = value;
	}

}
