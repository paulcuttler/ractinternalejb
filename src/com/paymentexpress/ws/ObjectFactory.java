package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.paymentexpress.ws package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: com.paymentexpress.ws
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ArrayOfNameValueField }
	 * 
	 */
	public ArrayOfNameValueField createArrayOfNameValueField() {
		return new ArrayOfNameValueField();
	}

	/**
	 * Create an instance of {@link TransactionDetails }
	 * 
	 */
	public TransactionDetails createTransactionDetails() {
		return new TransactionDetails();
	}

	/**
	 * Create an instance of {@link ArrayOfRiskRuleMatch }
	 * 
	 */
	public ArrayOfRiskRuleMatch createArrayOfRiskRuleMatch() {
		return new ArrayOfRiskRuleMatch();
	}

	/**
	 * Create an instance of {@link RiskRuleMatch }
	 * 
	 */
	public RiskRuleMatch createRiskRuleMatch() {
		return new RiskRuleMatch();
	}

	/**
	 * Create an instance of {@link SubmitTransaction }
	 * 
	 */
	public SubmitTransaction createSubmitTransaction() {
		return new SubmitTransaction();
	}

	/**
	 * Create an instance of {@link NameValueField }
	 * 
	 */
	public NameValueField createNameValueField() {
		return new NameValueField();
	}

	/**
	 * Create an instance of {@link SubmitTransactionResponse }
	 * 
	 */
	public SubmitTransactionResponse createSubmitTransactionResponse() {
		return new SubmitTransactionResponse();
	}

	/**
	 * Create an instance of {@link SubmitTransaction2 }
	 * 
	 */
	public SubmitTransaction2 createSubmitTransaction2() {
		return new SubmitTransaction2();
	}

	/**
	 * Create an instance of {@link UpdateCard }
	 * 
	 */
	public UpdateCard createUpdateCard() {
		return new UpdateCard();
	}

	/**
	 * Create an instance of {@link EnrolmentCheckResult }
	 * 
	 */
	public EnrolmentCheckResult createEnrolmentCheckResult() {
		return new EnrolmentCheckResult();
	}

	/**
	 * Create an instance of {@link TransactionResult2 }
	 * 
	 */
	public TransactionResult2 createTransactionResult2() {
		return new TransactionResult2();
	}

	/**
	 * Create an instance of {@link GetStatus }
	 * 
	 */
	public GetStatus createGetStatus() {
		return new GetStatus();
	}

	/**
	 * Create an instance of {@link Check3DsEnrollmentResponse }
	 * 
	 */
	public Check3DsEnrollmentResponse createCheck3DsEnrollmentResponse() {
		return new Check3DsEnrollmentResponse();
	}

	/**
	 * Create an instance of {@link Check3DsEnrollment }
	 * 
	 */
	public Check3DsEnrollment createCheck3DsEnrollment() {
		return new Check3DsEnrollment();
	}

	/**
	 * Create an instance of {@link GetStatus2 }
	 * 
	 */
	public GetStatus2 createGetStatus2() {
		return new GetStatus2();
	}

	/**
	 * Create an instance of {@link SubmitTransaction2Response }
	 * 
	 */
	public SubmitTransaction2Response createSubmitTransaction2Response() {
		return new SubmitTransaction2Response();
	}

	/**
	 * Create an instance of {@link GetStatusResponse }
	 * 
	 */
	public GetStatusResponse createGetStatusResponse() {
		return new GetStatusResponse();
	}

	/**
	 * Create an instance of {@link GetStatus2Response }
	 * 
	 */
	public GetStatus2Response createGetStatus2Response() {
		return new GetStatus2Response();
	}

	/**
	 * Create an instance of {@link UpdateCardResponse }
	 * 
	 */
	public UpdateCardResponse createUpdateCardResponse() {
		return new UpdateCardResponse();
	}

	/**
	 * Create an instance of {@link EnrolmentCheckRequest }
	 * 
	 */
	public EnrolmentCheckRequest createEnrolmentCheckRequest() {
		return new EnrolmentCheckRequest();
	}

}
