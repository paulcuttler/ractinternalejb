package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TransactionResult2 complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionResult2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acquirerReco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="acquirerResponseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="authCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="authorized" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billingId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardHolderHelpText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardHolderName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardHolderResponseDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardHolderResponseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currencyRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cvc2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateExpiry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateSettlement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dpsBillingId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dpsTxnRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="helpText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantHelpText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantResponseDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantResponseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="responseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statusRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="testMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iccData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardNumber2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issuerCountryId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnMac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cvc2ResultCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riskRuleMatches" type="{http://PaymentExpress.com}ArrayOfRiskRuleMatch" minOccurs="0"/>
 *         &lt;element name="extendedData" type="{http://PaymentExpress.com}ArrayOfNameValueField" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionResult2", propOrder = { "acquirerReco",
		"acquirerResponseText", "amount", "authCode", "authorized", "billingId",
		"cardHolderHelpText", "cardHolderName", "cardHolderResponseDescription",
		"cardHolderResponseText", "cardName", "cardNumber", "currencyId",
		"currencyName", "currencyRate", "cvc2", "dateExpiry", "dateSettlement",
		"dpsBillingId", "dpsTxnRef", "helpText", "merchantHelpText",
		"merchantReference", "merchantResponseDescription", "merchantResponseText",
		"reco", "responseText", "retry", "statusRequired", "testMode", "txnRef",
		"txnType", "iccData", "cardNumber2", "issuerCountryId", "txnMac",
		"cvc2ResultCode", "riskRuleMatches", "extendedData" })
public class TransactionResult2 {

	protected String acquirerReco;
	protected String acquirerResponseText;
	protected String amount;
	protected String authCode;
	protected String authorized;
	protected String billingId;
	protected String cardHolderHelpText;
	protected String cardHolderName;
	protected String cardHolderResponseDescription;
	protected String cardHolderResponseText;
	protected String cardName;
	protected String cardNumber;
	protected String currencyId;
	protected String currencyName;
	protected String currencyRate;
	protected String cvc2;
	protected String dateExpiry;
	protected String dateSettlement;
	protected String dpsBillingId;
	protected String dpsTxnRef;
	protected String helpText;
	protected String merchantHelpText;
	protected String merchantReference;
	protected String merchantResponseDescription;
	protected String merchantResponseText;
	protected String reco;
	protected String responseText;
	protected String retry;
	protected String statusRequired;
	protected String testMode;
	protected String txnRef;
	protected String txnType;
	protected String iccData;
	protected String cardNumber2;
	protected String issuerCountryId;
	protected String txnMac;
	protected String cvc2ResultCode;
	protected ArrayOfRiskRuleMatch riskRuleMatches;
	protected ArrayOfNameValueField extendedData;

	/**
	 * Gets the value of the acquirerReco property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcquirerReco() {
		return acquirerReco;
	}

	/**
	 * Sets the value of the acquirerReco property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAcquirerReco(String value) {
		this.acquirerReco = value;
	}

	/**
	 * Gets the value of the acquirerResponseText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAcquirerResponseText() {
		return acquirerResponseText;
	}

	/**
	 * Sets the value of the acquirerResponseText property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAcquirerResponseText(String value) {
		this.acquirerResponseText = value;
	}

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAmount(String value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the authCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthCode() {
		return authCode;
	}

	/**
	 * Sets the value of the authCode property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAuthCode(String value) {
		this.authCode = value;
	}

	/**
	 * Gets the value of the authorized property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAuthorized() {
		return authorized;
	}

	/**
	 * Sets the value of the authorized property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAuthorized(String value) {
		this.authorized = value;
	}

	/**
	 * Gets the value of the billingId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillingId() {
		return billingId;
	}

	/**
	 * Sets the value of the billingId property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setBillingId(String value) {
		this.billingId = value;
	}

	/**
	 * Gets the value of the cardHolderHelpText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardHolderHelpText() {
		return cardHolderHelpText;
	}

	/**
	 * Sets the value of the cardHolderHelpText property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardHolderHelpText(String value) {
		this.cardHolderHelpText = value;
	}

	/**
	 * Gets the value of the cardHolderName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * Sets the value of the cardHolderName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardHolderName(String value) {
		this.cardHolderName = value;
	}

	/**
	 * Gets the value of the cardHolderResponseDescription property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardHolderResponseDescription() {
		return cardHolderResponseDescription;
	}

	/**
	 * Sets the value of the cardHolderResponseDescription property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardHolderResponseDescription(String value) {
		this.cardHolderResponseDescription = value;
	}

	/**
	 * Gets the value of the cardHolderResponseText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardHolderResponseText() {
		return cardHolderResponseText;
	}

	/**
	 * Sets the value of the cardHolderResponseText property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardHolderResponseText(String value) {
		this.cardHolderResponseText = value;
	}

	/**
	 * Gets the value of the cardName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardName() {
		return cardName;
	}

	/**
	 * Sets the value of the cardName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardName(String value) {
		this.cardName = value;
	}

	/**
	 * Gets the value of the cardNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * Sets the value of the cardNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardNumber(String value) {
		this.cardNumber = value;
	}

	/**
	 * Gets the value of the currencyId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyId() {
		return currencyId;
	}

	/**
	 * Sets the value of the currencyId property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCurrencyId(String value) {
		this.currencyId = value;
	}

	/**
	 * Gets the value of the currencyName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyName() {
		return currencyName;
	}

	/**
	 * Sets the value of the currencyName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCurrencyName(String value) {
		this.currencyName = value;
	}

	/**
	 * Gets the value of the currencyRate property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrencyRate() {
		return currencyRate;
	}

	/**
	 * Sets the value of the currencyRate property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCurrencyRate(String value) {
		this.currencyRate = value;
	}

	/**
	 * Gets the value of the cvc2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCvc2() {
		return cvc2;
	}

	/**
	 * Sets the value of the cvc2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCvc2(String value) {
		this.cvc2 = value;
	}

	/**
	 * Gets the value of the dateExpiry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateExpiry() {
		return dateExpiry;
	}

	/**
	 * Sets the value of the dateExpiry property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDateExpiry(String value) {
		this.dateExpiry = value;
	}

	/**
	 * Gets the value of the dateSettlement property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateSettlement() {
		return dateSettlement;
	}

	/**
	 * Sets the value of the dateSettlement property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDateSettlement(String value) {
		this.dateSettlement = value;
	}

	/**
	 * Gets the value of the dpsBillingId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDpsBillingId() {
		return dpsBillingId;
	}

	/**
	 * Sets the value of the dpsBillingId property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDpsBillingId(String value) {
		this.dpsBillingId = value;
	}

	/**
	 * Gets the value of the dpsTxnRef property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDpsTxnRef() {
		return dpsTxnRef;
	}

	/**
	 * Sets the value of the dpsTxnRef property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDpsTxnRef(String value) {
		this.dpsTxnRef = value;
	}

	/**
	 * Gets the value of the helpText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getHelpText() {
		return helpText;
	}

	/**
	 * Sets the value of the helpText property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setHelpText(String value) {
		this.helpText = value;
	}

	/**
	 * Gets the value of the merchantHelpText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMerchantHelpText() {
		return merchantHelpText;
	}

	/**
	 * Sets the value of the merchantHelpText property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMerchantHelpText(String value) {
		this.merchantHelpText = value;
	}

	/**
	 * Gets the value of the merchantReference property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMerchantReference() {
		return merchantReference;
	}

	/**
	 * Sets the value of the merchantReference property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMerchantReference(String value) {
		this.merchantReference = value;
	}

	/**
	 * Gets the value of the merchantResponseDescription property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMerchantResponseDescription() {
		return merchantResponseDescription;
	}

	/**
	 * Sets the value of the merchantResponseDescription property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMerchantResponseDescription(String value) {
		this.merchantResponseDescription = value;
	}

	/**
	 * Gets the value of the merchantResponseText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMerchantResponseText() {
		return merchantResponseText;
	}

	/**
	 * Sets the value of the merchantResponseText property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMerchantResponseText(String value) {
		this.merchantResponseText = value;
	}

	/**
	 * Gets the value of the reco property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getReco() {
		return reco;
	}

	/**
	 * Sets the value of the reco property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setReco(String value) {
		this.reco = value;
	}

	/**
	 * Gets the value of the responseText property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getResponseText() {
		return responseText;
	}

	/**
	 * Sets the value of the responseText property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setResponseText(String value) {
		this.responseText = value;
	}

	/**
	 * Gets the value of the retry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getRetry() {
		return retry;
	}

	/**
	 * Sets the value of the retry property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setRetry(String value) {
		this.retry = value;
	}

	/**
	 * Gets the value of the statusRequired property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatusRequired() {
		return statusRequired;
	}

	/**
	 * Sets the value of the statusRequired property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setStatusRequired(String value) {
		this.statusRequired = value;
	}

	/**
	 * Gets the value of the testMode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTestMode() {
		return testMode;
	}

	/**
	 * Sets the value of the testMode property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTestMode(String value) {
		this.testMode = value;
	}

	/**
	 * Gets the value of the txnRef property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnRef() {
		return txnRef;
	}

	/**
	 * Sets the value of the txnRef property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnRef(String value) {
		this.txnRef = value;
	}

	/**
	 * Gets the value of the txnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the value of the txnType property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnType(String value) {
		this.txnType = value;
	}

	/**
	 * Gets the value of the iccData property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIccData() {
		return iccData;
	}

	/**
	 * Sets the value of the iccData property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setIccData(String value) {
		this.iccData = value;
	}

	/**
	 * Gets the value of the cardNumber2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardNumber2() {
		return cardNumber2;
	}

	/**
	 * Sets the value of the cardNumber2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardNumber2(String value) {
		this.cardNumber2 = value;
	}

	/**
	 * Gets the value of the issuerCountryId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIssuerCountryId() {
		return issuerCountryId;
	}

	/**
	 * Sets the value of the issuerCountryId property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setIssuerCountryId(String value) {
		this.issuerCountryId = value;
	}

	/**
	 * Gets the value of the txnMac property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnMac() {
		return txnMac;
	}

	/**
	 * Sets the value of the txnMac property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnMac(String value) {
		this.txnMac = value;
	}

	/**
	 * Gets the value of the cvc2ResultCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCvc2ResultCode() {
		return cvc2ResultCode;
	}

	/**
	 * Sets the value of the cvc2ResultCode property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCvc2ResultCode(String value) {
		this.cvc2ResultCode = value;
	}

	/**
	 * Gets the value of the riskRuleMatches property.
	 * 
	 * @return possible object is {@link ArrayOfRiskRuleMatch }
	 * 
	 */
	public ArrayOfRiskRuleMatch getRiskRuleMatches() {
		return riskRuleMatches;
	}

	/**
	 * Sets the value of the riskRuleMatches property.
	 * 
	 * @param value
	 *          allowed object is {@link ArrayOfRiskRuleMatch }
	 * 
	 */
	public void setRiskRuleMatches(ArrayOfRiskRuleMatch value) {
		this.riskRuleMatches = value;
	}

	/**
	 * Gets the value of the extendedData property.
	 * 
	 * @return possible object is {@link ArrayOfNameValueField }
	 * 
	 */
	public ArrayOfNameValueField getExtendedData() {
		return extendedData;
	}

	/**
	 * Sets the value of the extendedData property.
	 * 
	 * @param value
	 *          allowed object is {@link ArrayOfNameValueField }
	 * 
	 */
	public void setExtendedData(ArrayOfNameValueField value) {
		this.extendedData = value;
	}

}
