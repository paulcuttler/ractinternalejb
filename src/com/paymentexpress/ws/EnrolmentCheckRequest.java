package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for EnrolmentCheckRequest complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="EnrolmentCheckRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateExpiry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnrolmentCheckRequest", propOrder = { "amount", "cardNumber",
		"dateExpiry", "txnDescription", "txnRef", "currency" })
public class EnrolmentCheckRequest {

	protected String amount;
	protected String cardNumber;
	protected String dateExpiry;
	protected String txnDescription;
	protected String txnRef;
	protected String currency;

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAmount(String value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the cardNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * Sets the value of the cardNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardNumber(String value) {
		this.cardNumber = value;
	}

	/**
	 * Gets the value of the dateExpiry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateExpiry() {
		return dateExpiry;
	}

	/**
	 * Sets the value of the dateExpiry property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDateExpiry(String value) {
		this.dateExpiry = value;
	}

	/**
	 * Gets the value of the txnDescription property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnDescription() {
		return txnDescription;
	}

	/**
	 * Sets the value of the txnDescription property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnDescription(String value) {
		this.txnDescription = value;
	}

	/**
	 * Gets the value of the txnRef property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnRef() {
		return txnRef;
	}

	/**
	 * Sets the value of the txnRef property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnRef(String value) {
		this.txnRef = value;
	}

	/**
	 * Gets the value of the currency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * Sets the value of the currency property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCurrency(String value) {
		this.currency = value;
	}

}
