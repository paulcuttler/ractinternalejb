package com.paymentexpress.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TransactionDetails complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billingId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardHolderName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cvc2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateExpiry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dpsBillingId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dpsTxnRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enableAddBillCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enablePaxInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inputCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxCarrier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxCarrier2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxCarrier3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxCarrier4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxDateDepart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxDate2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxDate3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxDate4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxTime1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxTime2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxTime3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxTime4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxLeg1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxLeg2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxLeg3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxLeg4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxClass1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxClass2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxClass3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxClass4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxStopOverCode1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxStopOverCode2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxStopOverCode3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxStopOverCode4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxFareBasis1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxFareBasis2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxFareBasis3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxFareBasis4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxFlightNumber1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxFlightNumber2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxFlightNumber3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxFlightNumber4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxOrigin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paxTravelAgentInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnData1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnData2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnData3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="txnType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateStart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issueNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enableAvsData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="avsAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="avsPostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="avsStreetAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="enable3DSecure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paRes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iccData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardNumber2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="track2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cvc2Presence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extendedData" type="{http://PaymentExpress.com}ArrayOfNameValueField" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionDetails", propOrder = { "amount", "billingId",
		"cardHolderName", "cardNumber", "clientInfo", "cvc2", "dateExpiry",
		"dpsBillingId", "dpsTxnRef", "enableAddBillCard", "enablePaxInfo",
		"inputCurrency", "merchantReference", "paxCarrier", "paxCarrier2",
		"paxCarrier3", "paxCarrier4", "paxDateDepart", "paxDate2", "paxDate3",
		"paxDate4", "paxTime1", "paxTime2", "paxTime3", "paxTime4", "paxLeg1",
		"paxLeg2", "paxLeg3", "paxLeg4", "paxClass1", "paxClass2", "paxClass3",
		"paxClass4", "paxStopOverCode1", "paxStopOverCode2", "paxStopOverCode3",
		"paxStopOverCode4", "paxFareBasis1", "paxFareBasis2", "paxFareBasis3",
		"paxFareBasis4", "paxFlightNumber1", "paxFlightNumber2",
		"paxFlightNumber3", "paxFlightNumber4", "paxName", "paxOrigin",
		"paxTicketNumber", "paxTravelAgentInfo", "txnData1", "txnData2",
		"txnData3", "txnRef", "txnType", "dateStart", "issueNumber",
		"enableAvsData", "avsAction", "avsPostCode", "avsStreetAddress",
		"enable3DSecure", "paRes", "clientType", "iccData", "deviceId",
		"cardNumber2", "track2", "cvc2Presence", "extendedData" })
public class TransactionDetails {

	protected String amount;
	protected String billingId;
	protected String cardHolderName;
	protected String cardNumber;
	protected String clientInfo;
	protected String cvc2;
	protected String dateExpiry;
	protected String dpsBillingId;
	protected String dpsTxnRef;
	protected String enableAddBillCard;
	protected String enablePaxInfo;
	protected String inputCurrency;
	protected String merchantReference;
	protected String paxCarrier;
	protected String paxCarrier2;
	protected String paxCarrier3;
	protected String paxCarrier4;
	protected String paxDateDepart;
	protected String paxDate2;
	protected String paxDate3;
	protected String paxDate4;
	protected String paxTime1;
	protected String paxTime2;
	protected String paxTime3;
	protected String paxTime4;
	protected String paxLeg1;
	protected String paxLeg2;
	protected String paxLeg3;
	protected String paxLeg4;
	protected String paxClass1;
	protected String paxClass2;
	protected String paxClass3;
	protected String paxClass4;
	protected String paxStopOverCode1;
	protected String paxStopOverCode2;
	protected String paxStopOverCode3;
	protected String paxStopOverCode4;
	protected String paxFareBasis1;
	protected String paxFareBasis2;
	protected String paxFareBasis3;
	protected String paxFareBasis4;
	protected String paxFlightNumber1;
	protected String paxFlightNumber2;
	protected String paxFlightNumber3;
	protected String paxFlightNumber4;
	protected String paxName;
	protected String paxOrigin;
	protected String paxTicketNumber;
	protected String paxTravelAgentInfo;
	protected String txnData1;
	protected String txnData2;
	protected String txnData3;
	protected String txnRef;
	protected String txnType;
	protected String dateStart;
	protected String issueNumber;
	protected String enableAvsData;
	protected String avsAction;
	protected String avsPostCode;
	protected String avsStreetAddress;
	protected String enable3DSecure;
	protected String paRes;
	protected String clientType;
	protected String iccData;
	protected String deviceId;
	protected String cardNumber2;
	protected String track2;
	protected String cvc2Presence;
	protected ArrayOfNameValueField extendedData;

	/**
	 * Gets the value of the amount property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the value of the amount property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAmount(String value) {
		this.amount = value;
	}

	/**
	 * Gets the value of the billingId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillingId() {
		return billingId;
	}

	/**
	 * Sets the value of the billingId property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setBillingId(String value) {
		this.billingId = value;
	}

	/**
	 * Gets the value of the cardHolderName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * Sets the value of the cardHolderName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardHolderName(String value) {
		this.cardHolderName = value;
	}

	/**
	 * Gets the value of the cardNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * Sets the value of the cardNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardNumber(String value) {
		this.cardNumber = value;
	}

	/**
	 * Gets the value of the clientInfo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClientInfo() {
		return clientInfo;
	}

	/**
	 * Sets the value of the clientInfo property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setClientInfo(String value) {
		this.clientInfo = value;
	}

	/**
	 * Gets the value of the cvc2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCvc2() {
		return cvc2;
	}

	/**
	 * Sets the value of the cvc2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCvc2(String value) {
		this.cvc2 = value;
	}

	/**
	 * Gets the value of the dateExpiry property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateExpiry() {
		return dateExpiry;
	}

	/**
	 * Sets the value of the dateExpiry property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDateExpiry(String value) {
		this.dateExpiry = value;
	}

	/**
	 * Gets the value of the dpsBillingId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDpsBillingId() {
		return dpsBillingId;
	}

	/**
	 * Sets the value of the dpsBillingId property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDpsBillingId(String value) {
		this.dpsBillingId = value;
	}

	/**
	 * Gets the value of the dpsTxnRef property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDpsTxnRef() {
		return dpsTxnRef;
	}

	/**
	 * Sets the value of the dpsTxnRef property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDpsTxnRef(String value) {
		this.dpsTxnRef = value;
	}

	/**
	 * Gets the value of the enableAddBillCard property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnableAddBillCard() {
		return enableAddBillCard;
	}

	/**
	 * Sets the value of the enableAddBillCard property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setEnableAddBillCard(String value) {
		this.enableAddBillCard = value;
	}

	/**
	 * Gets the value of the enablePaxInfo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnablePaxInfo() {
		return enablePaxInfo;
	}

	/**
	 * Sets the value of the enablePaxInfo property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setEnablePaxInfo(String value) {
		this.enablePaxInfo = value;
	}

	/**
	 * Gets the value of the inputCurrency property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getInputCurrency() {
		return inputCurrency;
	}

	/**
	 * Sets the value of the inputCurrency property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setInputCurrency(String value) {
		this.inputCurrency = value;
	}

	/**
	 * Gets the value of the merchantReference property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMerchantReference() {
		return merchantReference;
	}

	/**
	 * Sets the value of the merchantReference property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setMerchantReference(String value) {
		this.merchantReference = value;
	}

	/**
	 * Gets the value of the paxCarrier property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxCarrier() {
		return paxCarrier;
	}

	/**
	 * Sets the value of the paxCarrier property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxCarrier(String value) {
		this.paxCarrier = value;
	}

	/**
	 * Gets the value of the paxCarrier2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxCarrier2() {
		return paxCarrier2;
	}

	/**
	 * Sets the value of the paxCarrier2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxCarrier2(String value) {
		this.paxCarrier2 = value;
	}

	/**
	 * Gets the value of the paxCarrier3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxCarrier3() {
		return paxCarrier3;
	}

	/**
	 * Sets the value of the paxCarrier3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxCarrier3(String value) {
		this.paxCarrier3 = value;
	}

	/**
	 * Gets the value of the paxCarrier4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxCarrier4() {
		return paxCarrier4;
	}

	/**
	 * Sets the value of the paxCarrier4 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxCarrier4(String value) {
		this.paxCarrier4 = value;
	}

	/**
	 * Gets the value of the paxDateDepart property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxDateDepart() {
		return paxDateDepart;
	}

	/**
	 * Sets the value of the paxDateDepart property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxDateDepart(String value) {
		this.paxDateDepart = value;
	}

	/**
	 * Gets the value of the paxDate2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxDate2() {
		return paxDate2;
	}

	/**
	 * Sets the value of the paxDate2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxDate2(String value) {
		this.paxDate2 = value;
	}

	/**
	 * Gets the value of the paxDate3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxDate3() {
		return paxDate3;
	}

	/**
	 * Sets the value of the paxDate3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxDate3(String value) {
		this.paxDate3 = value;
	}

	/**
	 * Gets the value of the paxDate4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxDate4() {
		return paxDate4;
	}

	/**
	 * Sets the value of the paxDate4 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxDate4(String value) {
		this.paxDate4 = value;
	}

	/**
	 * Gets the value of the paxTime1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxTime1() {
		return paxTime1;
	}

	/**
	 * Sets the value of the paxTime1 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxTime1(String value) {
		this.paxTime1 = value;
	}

	/**
	 * Gets the value of the paxTime2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxTime2() {
		return paxTime2;
	}

	/**
	 * Sets the value of the paxTime2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxTime2(String value) {
		this.paxTime2 = value;
	}

	/**
	 * Gets the value of the paxTime3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxTime3() {
		return paxTime3;
	}

	/**
	 * Sets the value of the paxTime3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxTime3(String value) {
		this.paxTime3 = value;
	}

	/**
	 * Gets the value of the paxTime4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxTime4() {
		return paxTime4;
	}

	/**
	 * Sets the value of the paxTime4 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxTime4(String value) {
		this.paxTime4 = value;
	}

	/**
	 * Gets the value of the paxLeg1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxLeg1() {
		return paxLeg1;
	}

	/**
	 * Sets the value of the paxLeg1 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxLeg1(String value) {
		this.paxLeg1 = value;
	}

	/**
	 * Gets the value of the paxLeg2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxLeg2() {
		return paxLeg2;
	}

	/**
	 * Sets the value of the paxLeg2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxLeg2(String value) {
		this.paxLeg2 = value;
	}

	/**
	 * Gets the value of the paxLeg3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxLeg3() {
		return paxLeg3;
	}

	/**
	 * Sets the value of the paxLeg3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxLeg3(String value) {
		this.paxLeg3 = value;
	}

	/**
	 * Gets the value of the paxLeg4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxLeg4() {
		return paxLeg4;
	}

	/**
	 * Sets the value of the paxLeg4 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxLeg4(String value) {
		this.paxLeg4 = value;
	}

	/**
	 * Gets the value of the paxClass1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxClass1() {
		return paxClass1;
	}

	/**
	 * Sets the value of the paxClass1 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxClass1(String value) {
		this.paxClass1 = value;
	}

	/**
	 * Gets the value of the paxClass2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxClass2() {
		return paxClass2;
	}

	/**
	 * Sets the value of the paxClass2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxClass2(String value) {
		this.paxClass2 = value;
	}

	/**
	 * Gets the value of the paxClass3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxClass3() {
		return paxClass3;
	}

	/**
	 * Sets the value of the paxClass3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxClass3(String value) {
		this.paxClass3 = value;
	}

	/**
	 * Gets the value of the paxClass4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxClass4() {
		return paxClass4;
	}

	/**
	 * Sets the value of the paxClass4 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxClass4(String value) {
		this.paxClass4 = value;
	}

	/**
	 * Gets the value of the paxStopOverCode1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxStopOverCode1() {
		return paxStopOverCode1;
	}

	/**
	 * Sets the value of the paxStopOverCode1 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxStopOverCode1(String value) {
		this.paxStopOverCode1 = value;
	}

	/**
	 * Gets the value of the paxStopOverCode2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxStopOverCode2() {
		return paxStopOverCode2;
	}

	/**
	 * Sets the value of the paxStopOverCode2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxStopOverCode2(String value) {
		this.paxStopOverCode2 = value;
	}

	/**
	 * Gets the value of the paxStopOverCode3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxStopOverCode3() {
		return paxStopOverCode3;
	}

	/**
	 * Sets the value of the paxStopOverCode3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxStopOverCode3(String value) {
		this.paxStopOverCode3 = value;
	}

	/**
	 * Gets the value of the paxStopOverCode4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxStopOverCode4() {
		return paxStopOverCode4;
	}

	/**
	 * Sets the value of the paxStopOverCode4 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxStopOverCode4(String value) {
		this.paxStopOverCode4 = value;
	}

	/**
	 * Gets the value of the paxFareBasis1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxFareBasis1() {
		return paxFareBasis1;
	}

	/**
	 * Sets the value of the paxFareBasis1 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxFareBasis1(String value) {
		this.paxFareBasis1 = value;
	}

	/**
	 * Gets the value of the paxFareBasis2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxFareBasis2() {
		return paxFareBasis2;
	}

	/**
	 * Sets the value of the paxFareBasis2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxFareBasis2(String value) {
		this.paxFareBasis2 = value;
	}

	/**
	 * Gets the value of the paxFareBasis3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxFareBasis3() {
		return paxFareBasis3;
	}

	/**
	 * Sets the value of the paxFareBasis3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxFareBasis3(String value) {
		this.paxFareBasis3 = value;
	}

	/**
	 * Gets the value of the paxFareBasis4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxFareBasis4() {
		return paxFareBasis4;
	}

	/**
	 * Sets the value of the paxFareBasis4 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxFareBasis4(String value) {
		this.paxFareBasis4 = value;
	}

	/**
	 * Gets the value of the paxFlightNumber1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxFlightNumber1() {
		return paxFlightNumber1;
	}

	/**
	 * Sets the value of the paxFlightNumber1 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxFlightNumber1(String value) {
		this.paxFlightNumber1 = value;
	}

	/**
	 * Gets the value of the paxFlightNumber2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxFlightNumber2() {
		return paxFlightNumber2;
	}

	/**
	 * Sets the value of the paxFlightNumber2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxFlightNumber2(String value) {
		this.paxFlightNumber2 = value;
	}

	/**
	 * Gets the value of the paxFlightNumber3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxFlightNumber3() {
		return paxFlightNumber3;
	}

	/**
	 * Sets the value of the paxFlightNumber3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxFlightNumber3(String value) {
		this.paxFlightNumber3 = value;
	}

	/**
	 * Gets the value of the paxFlightNumber4 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxFlightNumber4() {
		return paxFlightNumber4;
	}

	/**
	 * Sets the value of the paxFlightNumber4 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxFlightNumber4(String value) {
		this.paxFlightNumber4 = value;
	}

	/**
	 * Gets the value of the paxName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * Sets the value of the paxName property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxName(String value) {
		this.paxName = value;
	}

	/**
	 * Gets the value of the paxOrigin property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxOrigin() {
		return paxOrigin;
	}

	/**
	 * Sets the value of the paxOrigin property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxOrigin(String value) {
		this.paxOrigin = value;
	}

	/**
	 * Gets the value of the paxTicketNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxTicketNumber() {
		return paxTicketNumber;
	}

	/**
	 * Sets the value of the paxTicketNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxTicketNumber(String value) {
		this.paxTicketNumber = value;
	}

	/**
	 * Gets the value of the paxTravelAgentInfo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaxTravelAgentInfo() {
		return paxTravelAgentInfo;
	}

	/**
	 * Sets the value of the paxTravelAgentInfo property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaxTravelAgentInfo(String value) {
		this.paxTravelAgentInfo = value;
	}

	/**
	 * Gets the value of the txnData1 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnData1() {
		return txnData1;
	}

	/**
	 * Sets the value of the txnData1 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnData1(String value) {
		this.txnData1 = value;
	}

	/**
	 * Gets the value of the txnData2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnData2() {
		return txnData2;
	}

	/**
	 * Sets the value of the txnData2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnData2(String value) {
		this.txnData2 = value;
	}

	/**
	 * Gets the value of the txnData3 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnData3() {
		return txnData3;
	}

	/**
	 * Sets the value of the txnData3 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnData3(String value) {
		this.txnData3 = value;
	}

	/**
	 * Gets the value of the txnRef property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnRef() {
		return txnRef;
	}

	/**
	 * Sets the value of the txnRef property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnRef(String value) {
		this.txnRef = value;
	}

	/**
	 * Gets the value of the txnType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * Sets the value of the txnType property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTxnType(String value) {
		this.txnType = value;
	}

	/**
	 * Gets the value of the dateStart property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDateStart() {
		return dateStart;
	}

	/**
	 * Sets the value of the dateStart property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDateStart(String value) {
		this.dateStart = value;
	}

	/**
	 * Gets the value of the issueNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIssueNumber() {
		return issueNumber;
	}

	/**
	 * Sets the value of the issueNumber property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setIssueNumber(String value) {
		this.issueNumber = value;
	}

	/**
	 * Gets the value of the enableAvsData property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnableAvsData() {
		return enableAvsData;
	}

	/**
	 * Sets the value of the enableAvsData property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setEnableAvsData(String value) {
		this.enableAvsData = value;
	}

	/**
	 * Gets the value of the avsAction property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAvsAction() {
		return avsAction;
	}

	/**
	 * Sets the value of the avsAction property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAvsAction(String value) {
		this.avsAction = value;
	}

	/**
	 * Gets the value of the avsPostCode property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAvsPostCode() {
		return avsPostCode;
	}

	/**
	 * Sets the value of the avsPostCode property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAvsPostCode(String value) {
		this.avsPostCode = value;
	}

	/**
	 * Gets the value of the avsStreetAddress property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getAvsStreetAddress() {
		return avsStreetAddress;
	}

	/**
	 * Sets the value of the avsStreetAddress property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setAvsStreetAddress(String value) {
		this.avsStreetAddress = value;
	}

	/**
	 * Gets the value of the enable3DSecure property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getEnable3DSecure() {
		return enable3DSecure;
	}

	/**
	 * Sets the value of the enable3DSecure property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setEnable3DSecure(String value) {
		this.enable3DSecure = value;
	}

	/**
	 * Gets the value of the paRes property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPaRes() {
		return paRes;
	}

	/**
	 * Sets the value of the paRes property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setPaRes(String value) {
		this.paRes = value;
	}

	/**
	 * Gets the value of the clientType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getClientType() {
		return clientType;
	}

	/**
	 * Sets the value of the clientType property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setClientType(String value) {
		this.clientType = value;
	}

	/**
	 * Gets the value of the iccData property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIccData() {
		return iccData;
	}

	/**
	 * Sets the value of the iccData property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setIccData(String value) {
		this.iccData = value;
	}

	/**
	 * Gets the value of the deviceId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * Sets the value of the deviceId property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setDeviceId(String value) {
		this.deviceId = value;
	}

	/**
	 * Gets the value of the cardNumber2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCardNumber2() {
		return cardNumber2;
	}

	/**
	 * Sets the value of the cardNumber2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCardNumber2(String value) {
		this.cardNumber2 = value;
	}

	/**
	 * Gets the value of the track2 property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getTrack2() {
		return track2;
	}

	/**
	 * Sets the value of the track2 property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setTrack2(String value) {
		this.track2 = value;
	}

	/**
	 * Gets the value of the cvc2Presence property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCvc2Presence() {
		return cvc2Presence;
	}

	/**
	 * Sets the value of the cvc2Presence property.
	 * 
	 * @param value
	 *          allowed object is {@link String }
	 * 
	 */
	public void setCvc2Presence(String value) {
		this.cvc2Presence = value;
	}

	/**
	 * Gets the value of the extendedData property.
	 * 
	 * @return possible object is {@link ArrayOfNameValueField }
	 * 
	 */
	public ArrayOfNameValueField getExtendedData() {
		return extendedData;
	}

	/**
	 * Sets the value of the extendedData property.
	 * 
	 * @param value
	 *          allowed object is {@link ArrayOfNameValueField }
	 * 
	 */
	public void setExtendedData(ArrayOfNameValueField value) {
		this.extendedData = value;
	}

}
