package com.ract.web.convert.client;

import com.ract.common.GenericException;
import com.ract.web.client.WebClient;
import java.util.Collection;
import javax.ejb.Remote;

@Remote
public abstract interface WebClientMgrRemote
{
  public abstract Collection<ClMaster> getClientList(WebClient paramWebClient)
    throws Exception;

	public Collection<ClMaster> getIdenticalClients(WebClient webClient) throws GenericException;  
  
//  public abstract WebClient getWebClient(Integer paramInteger)
//    throws Exception;
//
//  public abstract void updateWebClient(WebClient paramWebClient)
//    throws Exception;
}