package com.ract.web.convert.client;

import com.ract.common.GenericException;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.util.StringUtil;
import com.ract.web.client.CustomerMgrLocal;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.client.WebClient;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote({WebClientMgrRemote.class})
@Local({WebClientMgrLocal.class})
public class WebClientMgr implements WebClientMgrRemote
{

	@PersistenceContext
	EntityManager em;

	public Collection<ClMaster> getClientList(WebClient webClient)
			throws Exception
	{
		LogUtil.log(this.getClass(),"webClient="+webClient);		
		Collection<ClMaster> clientList = new ArrayList<ClMaster>();
		ClMaster tmpClient = null;		
		if (webClient.getBirthDate() != null)
		{
			Collection<ClMaster> dobList = getClientListByDOB(webClient);

		  logClientCount(dobList,"dobList");	

			Collection<ClMaster> exactMatches = new ArrayList<ClMaster>();
			for (Iterator<ClMaster> x = dobList.iterator(); x.hasNext();)
			{
				tmpClient = x.next();				
				if (tmpClient.getBirthDate() != null &&
						tmpClient.getBirthDate().equals(webClient.getBirthDate()))
				{
					exactMatches.add(tmpClient);
				}
			}

			if (!(exactMatches.isEmpty()))
			{
				dobList = exactMatches;
			}

			clientList.addAll(dobList);
			tmpClient = null;			
		}

		if (webClient.getHomePhone() != null ||
				webClient.getWorkPhone() != null ||
				webClient.getMobilePhone() != null)
		{
			Collection<ClMaster> phoneList = getClientListByPhone(webClient);
			if (!(phoneList.isEmpty()))
			{
				for (Iterator<ClMaster> i = phoneList.iterator(); i.hasNext();)
				{
					tmpClient = i.next();
					if (!clientList.contains(tmpClient))
					{
						clientList.add(tmpClient);
					}
				}
			}
		}

		tmpClient = null;
		Collection addressList = null;
		
		if(webClient.getResiStreet() != null &&
				webClient.getResiSuburb() != null)
		{
			addressList = getClientsByResidentialAddress(webClient);
		}
		else if(webClient.getPostStreet() != null &&
				    webClient.getPostSuburb() != null)
		{
			addressList = getClientsByPostalAddress(webClient);
		}
		
		if (addressList !=null && 
				!addressList.isEmpty())
		{
			for (Iterator i = addressList.iterator(); i.hasNext();)
			{
				tmpClient = (ClMaster) i.next();
				if (!clientList.contains(tmpClient))
				{
					clientList.add(tmpClient);
				}
			}

		}
	  logClientCount(clientList,"getClientList");		
		
		return clientList;
	}

	private Collection<ClMaster> getClientListByPhone(WebClient webClient)
			throws Exception
	{
		List clientList = null;
		
		boolean hasWorkPhone = false;
		boolean hasHomePhone = false;
		boolean hasMobilePhone = false;
		
		try
		{
			String queryString = "select e from ClMaster as e where e.initials like :initials + '%' and e.sex = :sex and e.clientStatus <> 'DUPLICATE' and e.groupId = :groupId";	
			//match on home phone if they have provided one
			if (webClient.getHomePhone() != null &&
					!webClient.getHomePhone().trim().equals(""))
			{
				try
				{
					NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_HOME, webClient.getHomePhone());
	        hasHomePhone = true; 
				}
				catch (Exception e)
				{
					LogUtil.warn(this.getClass(), "Unable to validate home phone "+webClient+":"+FileUtil.NEW_LINE+e.getMessage());
					hasHomePhone = false;
				}
			}
			//match on work phone if they have provided one
			if (webClient.getWorkPhone() != null &&
					!webClient.getWorkPhone().trim().equals(""))
			{
				try
				{
					NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_WORK, webClient.getWorkPhone());
	        hasWorkPhone = true; 
				}
				catch (Exception e)
				{
					LogUtil.warn(this.getClass(), "Unable to validate work phone "+webClient+":"+FileUtil.NEW_LINE+e.getMessage());
					hasWorkPhone = false;
				}
			}
      //match on mobile phone if they have provided one
			if (webClient.getMobilePhone() != null &&
					!webClient.getMobilePhone().trim().equals(""))
			{
				try
				{
					NumberUtil.validatePhoneNumber(NumberUtil.PHONE_TYPE_MOBILE, webClient.getMobilePhone());
					hasMobilePhone = true; 
				}
				catch (Exception e)
				{
					LogUtil.warn(this.getClass(), "Unable to validate mobile phone "+webClient+":"+FileUtil.NEW_LINE+e.getMessage());
					hasMobilePhone = false;
				}
			}
      
			if (hasHomePhone || hasWorkPhone || hasMobilePhone)
			{
				//open brackets
				queryString += " and (";

			
				if (hasHomePhone)
				{
					queryString += "homePhone = :home";
					if (hasWorkPhone)
					{
						queryString += " or workPhone = :work";				
					}
					//regardless or work phone add as an or as we have a home phone
					if (hasMobilePhone)
					{
						queryString += " or mobilePhone = :mobile";
					}
				}
				else //no home phone
				{
					if (hasWorkPhone)
					{
						queryString += "workPhone = :work";
						if (hasMobilePhone)
						{
							queryString += " or mobilePhone = :mobile";
						}					
					}
					else //no work phone
					{
						if (hasMobilePhone)
						{
							queryString += "mobilePhone = :mobile";
						}		
					}
				}
			
				//close brackets
				queryString += ")";
		
				LogUtil.log(this.getClass(), "getClientListByPhone="+queryString);
				
				Query query = this.em.createQuery(queryString);				
				
				if (hasHomePhone)
				{
				  query.setParameter("home", webClient.getHomePhone());
				}
				if (hasWorkPhone)
				{
				  query.setParameter("work", webClient.getWorkPhone());
				}
				if (hasMobilePhone)
				{
				  query.setParameter("mobile", webClient.getMobilePhone());
				}
				
				query.setParameter("initials", getInitials(webClient.getGivenNames()
						.trim()));
				query.setParameter("sex", getGender(webClient.getGender().trim()));			
				query.setParameter("groupId", new Boolean(false));
				
	  		clientList = query.getResultList();				
			}
			else //no phone number elements
			{
				clientList = new ArrayList<ClMaster>();
			}

		  logClientCount(clientList,"getClientListByPhone");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}
		return clientList;
	}

	private void logClientCount(Collection<ClMaster> clientList, String search)
	{
		if (clientList != null)
		{
			LogUtil.log(this.getClass(), search+ ": client list has "+clientList.size()+" records.");
		}
		else
		{
			LogUtil.log(this.getClass(), search+ ": client list is empty.");
		}
	}
	
	private Collection<ClMaster> getClientListByDOB(WebClient webClient)
			throws Exception
	{
		final String QL_DOB = "select e from ClMaster as e where e.surname = :surname and e.initials like :initials + '%' and(e.birthDate = :birthDate or e.birthDate is null) and e.sex = :sex and e.clientStatus <> 'DUPLICATE' and e.groupId = :groupId";		
		LogUtil.log(this.getClass(), "getClientListByDOB="+QL_DOB);		
		List clientList = null;

		if ((webClient.getSurname() == null) || (webClient.getSurname().equals("")))
		{
			throw new Exception("Surname is null or empty in getMatchingClientList");
		}
		if (webClient.getBirthDate() == null)
		{
			throw new Exception("Birth Date is null in getMatchingClients");
		}
		if ((webClient.getGender() == null) || (webClient.getGender().equals("")))
		{
			throw new Exception("Gender is null or empty in getMatchingClients");
		}
		try
		{
			Query query = this.em
					.createQuery(QL_DOB);

			String initials = getInitials(webClient.getGivenNames()
					.trim());
			String surname = webClient.getSurname().trim();
			boolean gender =  getGender(webClient.getGender().trim());
      LogUtil.log(this.getClass(), "initials="+initials);
      LogUtil.log(this.getClass(), "surname="+surname);
      LogUtil.log(this.getClass(), "gender="+gender);      
      LogUtil.log(this.getClass(), "birthDate="+webClient.getBirthDate());      
			query.setParameter("surname", surname);
			query.setParameter("initials", initials);
			query.setParameter("birthDate", webClient.getBirthDate());
			query.setParameter("sex",gender);
			query.setParameter("groupId", new Boolean(false));
			clientList = query.getResultList();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception(e);
		}
	  logClientCount(clientList,"getClientListByDOB");		
		return clientList;
	}

	private Collection<ClMaster> getClientsByResidentialAddress(
			WebClient webClient) throws GenericException
	{
		Collection clientList = null;

		final String QL_RESI_ADDR = "select e from ClMaster e, GnStsub g  where e.resiStsubid = g.stsubid and (e.resiStreetNo = :resiStreetNo or e.resiStreetNo is null)  and g.street like :street + '%' and g.suburb like :suburb + '%' and g.state = :state  and g.postcode = :postcode  and e.clientStatus <> 'DUPLICATE' and e.clientStatus <> 'ADDRESS UNKNOWN' and e.sex = :sex and e.groupId = :groupId";
		
		Query query = this.em
				.createQuery(QL_RESI_ADDR);

		LogUtil.log(this.getClass(), "getClientsByResidentialAddress="+QL_RESI_ADDR);
		
		String resiStreetChar = webClient.getResiStreetChar();

		Integer streetNumber = extractStreetNumber(resiStreetChar);

		query.setParameter("resiStreetNo", streetNumber);

		String street = webClient.getResiStreet();
		StringBuffer resiStreet = extractStreetName(street);
		query.setParameter("street", resiStreet.toString());

		String resiSuburb = webClient.getResiSuburb();
		if (resiSuburb == null)
		{
			throw new GenericException("Residential suburb may not be empty.");
		}
		LogUtil.log(this.getClass(),"resiSuburb="+resiSuburb);
		query.setParameter("suburb", resiSuburb);
		query.setParameter("state", "TAS");

		query.setParameter("sex", getGender(webClient.getGender() == null ? "" : webClient.getGender().trim()));
		query.setParameter("postcode", webClient.getPostPostcode());

		query.setParameter("groupId", new Boolean(false));
		clientList = query.getResultList();
	  logClientCount(clientList,"getClientsByResidentialAddress");
		return clientList;
	}
	
	public Collection<ClMaster> getIdenticalClients(WebClient webClient) throws GenericException
	{
		Collection clientList = null;
		final String QL_EXACT_MATCH = "select e from ClMaster e where e.resiStsubid = :resiStsubid and " +
		  "(e.resiStreetChar = :resiStreetChar or e.resiStreetChar is null) " +
		  " and (e.resiProperty = :resiProperty or e.resiProperty is null)" +			  
		  " and e.postStsubid = :postStsubid and (e.postStreetChar = :postStreetChar or e.postStreetChar is null)" +
		  " and (e.postProperty = :postProperty or e.postProperty is null)" +		  
		  "  and e.clientStatus <> 'DUPLICATE' and "+
		  " e.clientStatus <> 'ADDRESS UNKNOWN' and e.sex = :sex and e.clientTitle = :clientTitle and "+
		  " e.surname = :surname and e.initials = :initials and e.birthDate = :birthDate  and e.groupId = :groupId";

		Query query = em.createQuery(QL_EXACT_MATCH);
    
		boolean gender = getGender(webClient.getGender().trim());		
		String initials = getInitials(webClient.getGivenNames());
		query.setParameter("resiStsubid", webClient.getResiStsubid());
		query.setParameter("resiStreetChar", webClient.getResiStreetChar());
		query.setParameter("resiProperty", webClient.getResiProperty());
		query.setParameter("postStsubid", webClient.getPostStsubid());
		query.setParameter("postStreetChar", webClient.getPostStreetChar());
		query.setParameter("postProperty", webClient.getPostProperty());
		query.setParameter("sex", gender);
		query.setParameter("birthDate", webClient.getBirthDate());
		query.setParameter("surname", webClient.getSurname());
		query.setParameter("initials", initials);
		query.setParameter("clientTitle", webClient.getTitle());
		query.setParameter("groupId", new Boolean(false));		
		clientList = query.getResultList();
	  logClientCount(clientList,"getIdenticalClients");		
		return clientList;
	}
	
	private Collection<ClMaster> getClientsByPostalAddress(WebClient webClient)throws GenericException
	{
		Collection clientList = null;

		final String QL_POST_ADDR = "select e from ClMaster e, GnStsub g  where e.postStsubid = g.stsubid and g.street like :street + '%' and g.suburb like :suburb + '%' and g.state = :state  and g.postcode = :postcode and e.clientStatus <> 'DUPLICATE' and e.clientStatus <> 'ADDRESS UNKNOWN' and e.sex = :sex and e.groupId = :groupId";
		LogUtil.log(this.getClass(), "getClientsByPostalAddress="+QL_POST_ADDR);		
		Query query = em.createQuery(QL_POST_ADDR);

		String street = webClient.getPostStreet();
		StringBuffer postStreet = extractStreetName(street);
		query.setParameter("street", postStreet.toString());

		String postSuburb = webClient.getPostSuburb();
		if (postSuburb == null)
		{
			throw new GenericException("Postal suburb may not be empty.");
		}
		boolean gender = getGender(webClient.getGender().trim());
		LogUtil.log(this.getClass(),"postSuburb="+postSuburb);
		LogUtil.log(this.getClass(),"gender="+gender);		
		LogUtil.log(this.getClass(),"postcode="+webClient.getPostPostcode());		
		query.setParameter("suburb", postSuburb);
		query.setParameter("state", "TAS");

		query.setParameter("sex", gender);
		query.setParameter("postcode", webClient.getPostPostcode());

		query.setParameter("groupId", new Boolean(false));
		clientList = query.getResultList();
	  logClientCount(clientList,"getClientsByPostalAddress");
		return clientList;

	}

	private StringBuffer extractStreetName(String street) throws GenericException
	{
		StringBuffer resiStreet = new StringBuffer();		
		if (street == null)
		{
			LogUtil.log(this.getClass(), "Residential street should not be empty.");
		}
		else
		{
			street = street.toUpperCase();
	
			System.out.println("street1=" + street);
	
			if (street.indexOf(" ") > 0)
			{
				String[] elem = street.split(" ");
				if (elem.length == 2)
				{
					resiStreet.append(elem[0]);
				}
				else
				{
					for (int x = 0; x < elem.length - 1; x++)
					{
						if (x > 0)
						{
							resiStreet.append(" ");
						}
						resiStreet.append(elem[x]);
					}
				}
	
				System.out.println("street2=" + resiStreet.toString());
			}
			System.out.println("street3=" + resiStreet.toString());
		}
		System.out.println("resiStreet" + resiStreet);		
		return resiStreet;
	}

	private Integer extractStreetNumber(String streetNo) throws GenericException
	{
		Integer streetNumber = null;
		String[] elem;
		if (streetNo != null &&
				streetNo.trim().length() > 0)
		{

			System.out.println("streetNo=" + streetNo);
	
			streetNo = StringUtil.replace(streetNo, ".", "");
			System.out.println("b4 streetNo=" + streetNo);
	
			if (streetNo.indexOf(",") != -1)
			{
				elem = streetNo.split(",");
				streetNo = elem[(elem.length - 1)];
			}
			if (streetNo.indexOf(" ") != -1)
			{
				//remove spaces. eg. 48 B --> 48B, 376 XXXXX --> 376XXXXX
				streetNo = StringUtil.replace(streetNo, " ", "");
			}
			if (streetNo.indexOf("/") != -1)
			{
				elem = streetNo.split("/");
				streetNo = elem[(elem.length - 1)];
			}
	
			streetNo = streetNo.trim();
	
			StringBuffer streetQual = new StringBuffer();
	
			for (int i = 0; i < streetNo.length(); i++)
			{
				char c = streetNo.charAt(i);
				if (Character.isDigit(c))
				{
					streetQual.append(c);
				}
			}
	
			System.out.println("after streetNo=" + streetQual);
			try
			{
				streetNumber = new Integer(streetQual.toString());
			}
			catch (NumberFormatException e)
			{
				LogUtil.warn(this.getClass(), "Unable to derive the street number from entry '" + streetQual + "'. "+streetNo+" "+e.getMessage());
			}
		
		}
		System.out.println("streetNumber=" + streetNumber);
		return streetNumber;
	}

	private String getInitials(String name)
	{
		StringBuffer initials = new StringBuffer();
		if (name == null)
			return null;
		int valid = 0;
		String[] names = name.toUpperCase().split(" ");
		for (int x = 0; x < 1; x++)
		{
			valid = 0;
			String elem = names[x];
			for (int y = 0; y < elem.length(); y++)
			{
				char c = elem.charAt(y);
				if ((Character.isLetter(c)) && (valid < 1))
				{
					valid++;
					initials.append(c);
				}
			}
		}

		System.out.println("initials=" + initials);
		return initials.toString().trim();
	}

	private Boolean getGender(String gString)
	{
		Boolean gender = null;
		if (gString.equalsIgnoreCase(WebClient.GENDER_MALE))
		{
			gender = new Boolean(true);
		}
		else
			if (gString.equalsIgnoreCase(WebClient.GENDER_FEMALE))
				gender = new Boolean(false);
		return gender;
	}

	public WebClient getWebClient(Integer webClientNo) throws Exception
	{
		WebClient clt = (WebClient) this.em.find(WebClient.class, webClientNo);
		return clt;
	}

	public void updateWebClient(WebClient clt) throws Exception
	{
		if (clt.getWebClientNo() == null)
		{
			throw new Exception(
					"Cannot update a WebClient without a web client no");
		}
		if (clt.getClientType() == null)
		{
			throw new Exception("Empty web client type");
		}
		this.em.merge(clt);
	}
}