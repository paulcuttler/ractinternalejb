package com.ract.web.convert.client;

import com.ract.util.DateTime;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="[cl-master]")
public class ClMaster
  implements Serializable
{

  @Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientNo == null) ? 0 : clientNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClMaster other = (ClMaster) obj;
		if (clientNo == null)
		{
			if (other.clientNo != null)
				return false;
		}
		else
			if (!clientNo.equals(other.clientNo))
				return false;
		return true;
	}

	@Id
  @Column(name="[client-no]")
  private Integer clientNo;

  @Column(name="`branch-no`")
  private Integer branchNo;

  @Column(name="`made-date`")
  private DateTime madeDate;

  @Column(name="`made-id`")
  private String madeId;

  @Column(name="`last-update`")
  private DateTime lastUpdate;

  @Column(name="`birth-date`")
  private DateTime birthDate;
  private Boolean sex;
  private String surname;

  @Column(name="`given-names`")
  private String givenNames;
  private String initials;

  @Column(name="`resi-property`")
  private String resiProperty;

  @Column(name="`resi-street-char`")
  private String resiStreetChar;

  @Column(name="`resi-street-no`")
  private Integer resiStreetNo;

  @Column(name="`resi-stsubid`")
  private Integer resiStsubid;

  @Column(name="`post-property`")
  private String postProperty;

  @Column(name="`post-street-char`")
  private String postStreetChar;

  @Column(name="`post-street-no`")
  private Integer postStreetNo;

  @Column(name="`post-stsubid`")
  private Integer postStsubid;

  @Column(name="`home-phone`")
  private String homePhone;

  @Column(name="`work-phone`")
  private String workPhone;

  @Column(name="`group-id`")
  private Boolean groupId;

  @Column(name="`client-title`")
  private String clientTitle;

  @Column(name="`postal-name`")
  private String postalName;

  @Column(name="`address-title`")
  private String addressTitle;

  @Column(name="`client-status`")
  private String clientStatus;

  @Column(name="`mobile-phone`")
  private String mobilePhone;
  private String email;

//  @Column(name="fax_number")
  @Transient
  private String faxNumber;

  @Column(name="resi_dpid")
  private String resiDpid;

  @Column(name="post_dpid")
  private String postDpid;
  private Boolean smsAllowed;
  private String phoneQuestion;
  private String phonePassword;

  public String toString()
  {
    StringBuffer desc = new StringBuffer();
    desc.append(this.clientNo);
    desc.append(": ");
    desc.append(this.clientTitle);
    desc.append(" ");
    desc.append(this.givenNames);
    desc.append(" ");
    desc.append(this.surname);
    desc.append(" ");
    desc.append(this.birthDate);
    desc.append(" ");
    desc.append(this.givenNames);
    desc.append(" ");
    desc.append(this.clientStatus);
    desc.append(" ");
    desc.append(this.resiStreetChar);
    desc.append(" ");
    desc.append(this.resiStsubid);
    return desc.toString();
  }

  public Integer getClientNo()
  {
    return this.clientNo;
  }

  public void setClientNo(Integer clientNo)
  {
    this.clientNo = clientNo;
  }

  public Integer getBranchNo()
  {
    return this.branchNo;
  }

  public void setBranchNo(Integer branchNo)
  {
    this.branchNo = branchNo;
  }

  public DateTime getMadeDate()
  {
    return this.madeDate;
  }

  public void setMadeDate(DateTime madeDate)
  {
    this.madeDate = madeDate;
  }

  public String getMadeId()
  {
    return this.madeId;
  }

  public void setMadeId(String madeId)
  {
    this.madeId = madeId;
  }

  public DateTime getLastUpdate()
  {
    return this.lastUpdate;
  }

  public void setLastUpdate(DateTime lastUpdate)
  {
    this.lastUpdate = lastUpdate;
  }

  public DateTime getBirthDate()
  {
    return this.birthDate;
  }

  public void setBirthDate(DateTime birthDate)
  {
    this.birthDate = birthDate;
  }

  public Boolean getSex()
  {
    return this.sex;
  }

  public void setSex(Boolean sex)
  {
    this.sex = sex;
  }

  public String getSurname()
  {
    return this.surname;
  }

  public void setSurname(String surname)
  {
    this.surname = surname;
  }

  public String getGivenNames()
  {
    return this.givenNames;
  }

  public void setGivenNames(String givenNames)
  {
    this.givenNames = givenNames;
  }

  public String getInitials()
  {
    return this.initials;
  }

  public void setInitials(String initials)
  {
    this.initials = initials;
  }

  public String getResiProperty()
  {
    return this.resiProperty;
  }

  public void setResiProperty(String resiProperty)
  {
    this.resiProperty = resiProperty;
  }

  public String getResiStreetChar()
  {
    return this.resiStreetChar;
  }

  public void setResiStreetChar(String resiStreetChar)
  {
    this.resiStreetChar = resiStreetChar;
  }

  public Integer getResiStreetNo()
  {
    return this.resiStreetNo;
  }

  public void setResiStreetNo(Integer resiStreetNo)
  {
    this.resiStreetNo = resiStreetNo;
  }

  public Integer getResiStsubid()
  {
    return this.resiStsubid;
  }

  public void setResiStsubid(Integer resiStsubid)
  {
    this.resiStsubid = resiStsubid;
  }

  public String getPostProperty()
  {
    return this.postProperty;
  }

  public void setPostProperty(String postProperty)
  {
    this.postProperty = postProperty;
  }

  public String getPostStreetChar()
  {
    return this.postStreetChar;
  }

  public void setPostStreetChar(String postStreetChar)
  {
    this.postStreetChar = postStreetChar;
  }

  public Integer getPostStreetNo()
  {
    return this.postStreetNo;
  }

  public void setPostStreetNo(Integer postStreetNo)
  {
    this.postStreetNo = postStreetNo;
  }

  public Integer getPostStsubid()
  {
    return this.postStsubid;
  }

  public void setPostStsubid(Integer postStsubid)
  {
    this.postStsubid = postStsubid;
  }

  public String getHomePhone()
  {
    return this.homePhone;
  }

  public void setHomePhone(String homePhone)
  {
    this.homePhone = homePhone;
  }

  public String getWorkPhone()
  {
    return this.workPhone;
  }

  public void setWorkPhone(String workPhone)
  {
    this.workPhone = workPhone;
  }

  public Boolean getGroupId()
  {
    return this.groupId;
  }

  public void setGroupId(Boolean groupId)
  {
    this.groupId = groupId;
  }

  public String getClientTitle()
  {
    return this.clientTitle;
  }

  public void setClientTitle(String clientTitle)
  {
    this.clientTitle = clientTitle;
  }

  public String getPostalName()
  {
    return this.postalName;
  }

  public void setPostalName(String postalName)
  {
    this.postalName = postalName;
  }

  public String getAddressTitle()
  {
    return this.addressTitle;
  }

  public void setAddressTitle(String addressTitle)
  {
    this.addressTitle = addressTitle;
  }

  public String getClientStatus()
  {
    return this.clientStatus;
  }

  public void setClientStatus(String clientStatus)
  {
    this.clientStatus = clientStatus;
  }

  public String getMobilePhone()
  {
    return this.mobilePhone;
  }

  public void setMobilePhone(String mobilePhone)
  {
    this.mobilePhone = mobilePhone;
  }

  public String getEmail()
  {
    return this.email;
  }

  public void setEmail(String email)
  {
    this.email = email;
  }

  public String getFaxNumber()
  {
    return this.faxNumber;
  }

  public void setFaxNumber(String faxNumber)
  {
    this.faxNumber = faxNumber;
  }

  public String getResiDpid()
  {
    return this.resiDpid;
  }

  public void setResiDpid(String resiDpid)
  {
    this.resiDpid = resiDpid;
  }

  public String getPostDpid()
  {
    return this.postDpid;
  }

  public void setPostDpid(String postDpid)
  {
    this.postDpid = postDpid;
  }

  public Boolean getSmsAllowed()
  {
    return this.smsAllowed;
  }

  public void setSmsAllowed(Boolean smsAllowed)
  {
    this.smsAllowed = smsAllowed;
  }

  public String getPhoneQuestion()
  {
    return this.phoneQuestion;
  }

  public void setPhoneQuestion(String phoneQuestion)
  {
    this.phoneQuestion = phoneQuestion;
  }

  public String getPhonePassword()
  {
    return this.phonePassword;
  }

  public void setPhonePassword(String phonePassword)
  {
    this.phonePassword = phonePassword;
  }


}