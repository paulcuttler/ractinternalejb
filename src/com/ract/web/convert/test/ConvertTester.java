package com.ract.web.convert.test;

import com.ract.common.ServiceLocator;
import com.ract.common.integration.RACTPropertiesProvider;
import com.ract.util.DateTime;
import com.ract.web.client.CustomerMgrLocal;
import com.ract.web.client.CustomerMgrRemote;
import com.ract.web.client.WebClient;
import com.ract.web.convert.client.ClMaster;
import com.ract.web.convert.client.WebClientMgrRemote;
import com.ract.web.convert.insurance.ConvertPolicyMgrRemote;
import com.ract.web.insurance.WebInsClient;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.Collection;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConvertTester
{
  ConvertPolicyMgrRemote bean;
  WebClientMgrRemote cltBean;
  CustomerMgrRemote custBean;  
  Integer quoteNo = new Integer(3);

  @Before
  public void setUp()
    throws Exception
  {
    System.out.println("Set up");
    connect();
  }

  public void testConvertPolicy()
    throws Exception
  {
    System.out.println("Testing policy conversion routine");
    Integer quoteNo = new Integer(6);
    String res = this.bean.convertPolicy(quoteNo, "dgk");
    System.out.println(res);
  }

  public void testFormatDate()
    throws Exception
  {
    DateTime dt = new DateTime();
    System.out.println(dt.formatLongDate());
  }

  @After
  public void tearDown() throws Exception
  {
    System.out.println("tearDown");
  }

  @Test
  public void testGetClientList() throws Exception
  {
    WebClient webClient = custBean.getWebClient(270);
    Collection list = this.cltBean.getClientList(webClient);

    for (Iterator x = list.iterator(); x.hasNext(); )
    {
      ClMaster rClt = (ClMaster)x.next();
      System.out.println(rClt);
    }
  }

  @Test
  public void testExactClientMatch() throws Exception
  {
    WebClient webClient = custBean.getWebClient(270);
    Collection list = this.cltBean.getIdenticalClients(webClient);

    System.out.println(list);
  }  
  
  private WebClient getWebClient()
  {
    WebClient wclt = new WebInsClient();
    try
    {
    	wclt.setTitle("Mrs");
    	wclt.setSurname("Cransfield");
      wclt.setGivenNames("Gail");
      wclt.setGender(WebClient.GENDER_FEMALE);
      wclt.setBirthDate(new DateTime("6/01/1963"));
      wclt.setResiStreetChar("3/249A");
      wclt.setResiStreet("Montagu Road");
      wclt.setResiSuburb("Smithton");
      wclt.setResiPostcode("7330");
      wclt.setMobilePhone("0408262279");
      wclt.setWorkPhone("0362326324");
      wclt.setHomePhone("0364522780");
//      wclt.setHomePhone(null);
      System.out.println(wclt);
    }
    catch (ParseException e)
    {
      e.printStackTrace();
    }
    return wclt;
  }

  private void connect() throws Exception
  {
    if ((this.bean == null) || (this.cltBean == null))
    {
      System.out.println("connecting");
      this.bean = 
        ((ConvertPolicyMgrRemote)ServiceLocator.getInstance()
        .getObject("ConvertPolicyMgr/remote"));
      this.cltBean = 
        ((WebClientMgrRemote)ServiceLocator.getInstance()
        .getObject("WebClientMgr/remote"));
      this.custBean = 
        ((CustomerMgrRemote)ServiceLocator.getInstance()
        .getObject("CustomerMgr/remote"));
    }
  }
}