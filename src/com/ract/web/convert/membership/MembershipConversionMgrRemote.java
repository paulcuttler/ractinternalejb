package com.ract.web.convert.membership;

import javax.ejb.Remote;

import com.ract.common.GenericException;

@Remote
public abstract interface MembershipConversionMgrRemote
{

	public String convertMembership(String transactionHeaderId, String userId)
			throws GenericException;
    
    public String obfuscate(String accountNo) throws Exception;

}