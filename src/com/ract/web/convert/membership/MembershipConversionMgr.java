package com.ract.web.convert.membership;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import com.ract.client.ClientMgrLocal;
import com.ract.client.ClientVO;
import com.ract.common.CommonMgrLocal;
import com.ract.common.GenericException;
import com.ract.common.ReferenceDataVO;
import com.ract.common.RollBackException;
import com.ract.common.SourceSystem;
import com.ract.common.SystemException;
import com.ract.common.SystemParameterVO;
import com.ract.common.ValidationException;
import com.ract.membership.MembershipEJBHelper;
import com.ract.membership.MembershipMgrLocal;
import com.ract.membership.MembershipTransactionTypeVO;
import com.ract.membership.MembershipTransactionVO;
import com.ract.membership.MembershipTypeVO;
import com.ract.membership.MembershipVO;
import com.ract.membership.ProductVO;
import com.ract.membership.TransactionGroup;
import com.ract.membership.TransactionMgrLocal;
import com.ract.payment.PayableItemVO;
import com.ract.payment.PaymentEJBHelper;
import com.ract.payment.PaymentException;
import com.ract.payment.PaymentMgr;
import com.ract.payment.PaymentMgrLocal;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.PaymentTypeVO;
import com.ract.payment.bank.BankAccount;
import com.ract.payment.bank.CreditCardAccount;
import com.ract.payment.bank.DebitAccount;
import com.ract.payment.directdebit.DirectDebitAuthority;
import com.ract.payment.directdebit.DirectDebitFrequency;
import com.ract.payment.ocv.OCVHelper;
import com.ract.payment.ocv.OCVTransaction;
import com.ract.payment.ocv.OCVTransactionMgrLocal;
import com.ract.payment.ocv.OCVTransactionRequest;
import com.ract.payment.ocv.OCVTransactionResponse;
import com.ract.user.UserMgrLocal;
import com.ract.util.CurrencyUtil;
import com.ract.util.DateTime;
import com.ract.util.DateUtil;
import com.ract.util.FileUtil;
import com.ract.util.Interval;
import com.ract.util.LogUtil;
import com.ract.util.NumberUtil;
import com.ract.web.convert.common.BaseConversionManager;
import com.ract.web.membership.WebMembershipMgrLocal;
import com.ract.web.membership.WebMembershipTransaction;
import com.ract.web.membership.WebMembershipTransactionContainer;
import com.ract.web.membership.WebMembershipTransactionHeader;
import com.ract.web.payment.WebMembershipPayment;

@Stateless
@Remote({ MembershipConversionMgrRemote.class })
@Local({ MembershipConversionMgrLocal.class })
public class MembershipConversionMgr extends BaseConversionManager implements MembershipConversionMgrRemote {

	public static final String USER_MEMBERSHIP_WEB = "MWEB";

	@EJB
	private WebMembershipMgrLocal webMembershipMgr;

	@EJB
	private UserMgrLocal userMgr;

	@EJB
	private MembershipMgrLocal membershipMgr;

	@EJB
	private CommonMgrLocal commonMgrLocal;

	@EJB
	private PaymentMgrLocal paymentMgrLocal;

	@EJB
	private OCVTransactionMgrLocal ocvTransactionMgr;

	@EJB
	private TransactionMgrLocal transactionMgrLocal;

	@EJB
	private PaymentTransactionMgrLocal paymentTransactionMgr;

	@EJB
	private ClientMgrLocal clientMgr;

	public String convertMembership(String transactionHeaderId, String userId) throws GenericException {

		String seqNumber = null;

		LogUtil.log(this.getClass(), "convertMembership start");
		LogUtil.log(this.getClass(), "transactionHeaderId=" + transactionHeaderId);
		LogUtil.log(this.getClass(), "userId=" + userId);

		// client should already be created
		// retrieve the web membership container
		WebMembershipTransactionContainer wmtc = webMembershipMgr.getWebMembershipTransactionContainer(new Integer(transactionHeaderId));

		// convert to a transaction group
		WebMembershipTransactionHeader wmth = wmtc.getWebMembershipTransactionHeader();

		if (!wmth.getConversionStatus().equals(WebMembershipTransactionHeader.STATUS_COVERED)) {
			throw new GenericException("Web membership may not be converted. The status is " + wmth.getConversionStatus());
		}

		WebMembershipTransaction wmt = wmtc.getPrimeAddressWebMembershipTransaction();

		WebMembershipPayment wmp = webMembershipMgr.getWebMembershipPayment(wmth.getTransactionHeaderId().toString());

		Integer clientNumber = wmt.getWebClient().getRactClientNo();
		if (clientNumber == null) {
			throw new GenericException("An RACT client has not been associated with the Web client.");
		}
		
		Collection membershipList = null;
		try {
			membershipList = membershipMgr.findMembershipByClientNumber(clientNumber);
		} catch (Exception e) {
			throw new GenericException(e.getMessage());
		}

		String transactionTypeCode = wmth.getTransactionGroupTypeCode();
		MembershipVO membership = null;
		if (membershipList != null && membershipList.size() > 0) {
			if (membershipList.size() == 1) {
				// look for cancelled and eligible rejoin
				membership = (MembershipVO) membershipList.iterator().next();
				LogUtil.debug(this.getClass(), "membership=" + membership);
				if (membership.getMembershipTypeCode().equals(MembershipTypeVO.MEMBERSHIP_TYPE_PERSONAL)) {

					try {
						if (membership.isGroupMember()) {
							throw new GenericException("RACT client is part of a group.");
						}
					} catch (Exception e1) {
						throw new GenericException(e1);
					}

					if (membership.isDirectDebit()) {
						throw new GenericException("Existing membership is payable by direct debit. Update payment method of membership to receipting to allow conversion.");
					}

					Interval postExpireRenewalPeriod = null;
					String memStatus = null;
					try {
						memStatus = membership.getBaseStatus();
						LogUtil.debug(this.getClass(), "memStatus=" + memStatus);
						// 5 months
						String postExpireRenewalPeriodSpec = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_MEMBERSHIP, SystemParameterVO.POST_EXPIRE_RENEWAL_PERIOD);
						postExpireRenewalPeriod = new Interval(postExpireRenewalPeriodSpec);
					} catch (Exception e) {
						throw new GenericException(e);
					}
					DateTime pastDate = new DateTime().subtract(postExpireRenewalPeriod);
					LogUtil.debug(this.getClass(), "pastDate=" + pastDate);
					LogUtil.debug(this.getClass(), "expiryDate=" + membership.getExpiryDate());
					boolean rejoinable = membership.getExpiryDate().beforeDay(pastDate);
					LogUtil.debug(this.getClass(), "rejoinable=" + rejoinable);

					if (memStatus.equals(MembershipVO.STATUS_CANCELLED) || (memStatus.equals(MembershipVO.STATUS_ACTIVE) && rejoinable) || memStatus.equals(MembershipVO.STATUS_UNDONE)) {
						LogUtil.debug(this.getClass(), "can rejoin");
						transactionTypeCode = MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN;
						// check if valid
						try {
							String response = membership.getInvalidForTransactionReason(transactionTypeCode);
							if (response != null) {
								throw new GenericException(response);
							}
						} catch (Exception e) {
							throw new GenericException(e.getMessage());
						}

					}
					// still create
					if (transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_CREATE)) {
						throw new GenericException("RACT client " + clientNumber + " already has a membership that is active or renewable. " + "You will need to either refund the money, associate another RACT client or manually convert the membership.");
					}

				}
			} else {
				throw new GenericException("There is more than one RACT membership attached to client " + clientNumber + ".");
			}

		}

		PayableItemVO payableItem = null;
		TransactionGroup transGroup = null;
		boolean paymentPerformed = false;
		try {

			transGroup = new TransactionGroup(transactionTypeCode, userMgr.getUser(userId));

			ClientVO client = clientMgr.getClient(clientNumber);

			transGroup.setTransactionDate(wmth.getCreateDate());

			if (membership != null) {
				String message = transGroup.addSelectedClient(membership);
				if (message != null && !message.trim().equals("")) {
					throw new GenericException(message);
				}
				transGroup.setContextClient(membership);
			} else {
				String message = transGroup.addSelectedClient(client);
				if (message != null && !message.trim().equals("")) {
					throw new GenericException(message);
				}
				transGroup.setContextClient(client);
			}

			transGroup.setMembershipTerm(new Interval(wmt.getMembershipTerm()));
			MembershipTypeVO memTypeVO = MembershipEJBHelper.getMembershipRefMgr().getMembershipType(wmt.getMembershipType());
			transGroup.setMembershipType(memTypeVO);
			transGroup.setMembershipProfile(wmt.getProfileCode());
			transGroup.setPreferredCommenceDate(wmt.getStartDate());
			transGroup.setExpiryDate(wmt.getEndDate());
			// TODO needed?
			// transGroup.setTransactionReason(reasonVO); //web reason?
			ProductVO product = MembershipEJBHelper.getMembershipRefMgr().getProduct(wmt.getProductCode());
			transGroup.setSelectedProduct(product);

			if (transactionTypeCode.equals(MembershipTransactionTypeVO.TRANSACTION_TYPE_REJOIN)) {
				// MEM-462 - default should be New
				// ReferenceDataVO defaultRejoinType = commonMgrLocal.getReferenceData(ReferenceDataVO.REF_TYPE_REJOIN_TYPE, ReferenceDataVO.REJOIN_TYPE_RESET_EXPIRY);
				ReferenceDataVO defaultRejoinType = commonMgrLocal.getReferenceData(ReferenceDataVO.REF_TYPE_REJOIN_TYPE, ReferenceDataVO.REJOIN_TYPE_NEW);
				transGroup.setRejoinType(defaultRejoinType);
			}

			// add a fee discount

			transGroup.createTransactionsForSelectedClients();
			transGroup.createTransactionFees();

			transGroup = webMembershipMgr.addWebDiscount(transGroup);

			transGroup.calculateTransactionFee();

			String transactionReference = wmt.getWebMembershipTransactionPK().getTransactionId().toString();
			MembershipTransactionVO memTransVO = transGroup.getMembershipTransactionForPA();
			memTransVO.setTransactionReference(transactionReference);
			// memTransVO.setSalesBranchCode(SALES_BRANCH_WEB);
			MembershipVO newMemVO = memTransVO.getNewMembership();
			newMemVO.setTransactionReference(transactionReference);
			newMemVO.setInceptionSalesBranch(SALES_BRANCH_WEB);

			// check amounts
			if (transGroup.getAmountPayable() != wmth.getAmountPayable().doubleValue()) {
				throw new GenericException("Amounts do not match. Web transaction was " + CurrencyUtil.formatDollarValue(wmth.getAmountPayable()) + " and" + " membership calculation was " + CurrencyUtil.formatDollarValue(transGroup.getAmountPayable()) + ".");
			}

			transGroup.logMembershipTransactions("conversion");
			
			String receiptNumber = null;
			
			String systemName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);
			
			if(wmp.getPaymentType().equals("DirectDebit")) {
				
				processDirectDebit(wmt, wmp, transGroup, newMemVO);
				Collection transactionList = transactionMgrLocal.processTransaction(transGroup);
				transactionMgrLocal.commit();
				
			} else if(wmp.getPaymentType().equals("PayNow")) {
				// create membership
				// create transaction, fees and discounts
				// create payable items, components and postings
				// create receptor pending
				Collection transactionList = transactionMgrLocal.processTransaction(transGroup);
				
				receiptNumber = processPayNow(wmth, wmp, clientNumber, payableItem, transGroup, transactionList); 
				
				if (!systemName.equals("Production"))
					transactionMgrLocal.commit();
					
			} else if(wmp.getPaymentType().equals("NoCharge")) {
				// Create membership and commit
				Collection transactionList = transactionMgrLocal.processTransaction(transGroup);
				transactionMgrLocal.commit();
				receiptNumber = "Not applicable";
			}
			
			
			// update wmt with membership and transaction ids
			wmt.setMembershipId(newMemVO.getMembershipID());
			wmt.setMembershipTransactionId(memTransVO.getTransactionID());

			// update transaction header conversion status and conversion date
			wmth.setConversionStatus(WebMembershipTransactionHeader.STATUS_CONVERTED);
			wmth.setConversionDate(new DateTime());
			wmth.setConversionReference(receiptNumber);
			wmth.setConversionUserId(userId);
			webMembershipMgr.updateWebMembershipTransactionHeader(wmth);
			webMembershipMgr.updateWebMembershipPayment(wmp);
			
			LogUtil.log(getClass(), "Finished updating webMembership");
			
		} catch (Exception e) {
			e.printStackTrace();
			// rollback user transaction, notification mgr and payment transaction mgr
			try {
				transactionMgrLocal.rollback();
			} catch (RemoteException e1) {
				LogUtil.fatal(this.getClass(), e);
			}

			// rollback receipt - in reality this doesn't do anything but should!
			try {
				paymentTransactionMgr.rollback();
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			// EJB exception will rollback any active EJB transactions
			throw new EJBException(e);
		}
		
		LogUtil.log(getClass(), "commit required transactions");
		
		try {
			LogUtil.log(getClass(), "commit payment transaction");
			
			paymentTransactionMgr.commit();
			
			LogUtil.log(getClass(), "finished commit payment transaction");
		} catch (Exception e) {
			LogUtil.fatal(this.getClass(), e);
		}

		String successMsg = "Successfully converted membership.";

		try {
			if (getWebReceiptingSystem().equals(SourceSystem.ECR)) {
				successMsg += "\nPlease allow a few minutes for a receipt to be created.";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LogUtil.log(getClass(), "complete membership conversion");
		
		return successMsg;
	}

	private String processPayNow(WebMembershipTransactionHeader wmth, WebMembershipPayment wmp, Integer clientNumber, PayableItemVO payableItem, TransactionGroup transGroup, Collection transactionList) throws RemoteException, Exception, RollBackException {
		String seqNumber;
		String receiptNumber;
		String sourceSystemReference = "";
		// avoid a database fetch by attaching the payable item to the transaction
		MembershipTransactionVO tmpTrans = null;
		for (Iterator<MembershipTransactionVO> i = transactionList.iterator(); i.hasNext() && payableItem == null;) {
			tmpTrans = i.next();
			payableItem = tmpTrans.getPayableItem();
			if (payableItem != null) {
				sourceSystemReference = tmpTrans.getNewMembership().getMembershipID().toString();
			}
		}
		seqNumber = payableItem.getPaymentMethodID();
		
		SourceSystem webPaymentSystem = getWebPaymentSystem();
		SourceSystem webReceiptingSystem = getWebReceiptingSystem();
		
		if (webPaymentSystem.equals(SourceSystem.DPS)) {
			 
			String systemName = commonMgrLocal.getSystemParameterValue(SystemParameterVO.CATEGORY_COMMON, SystemParameterVO.TYPE_SYSTEM);
			
			if (systemName.equals("Production")) {
				Map<String, String> response = payByDPS(transactionMgrLocal, transGroup.getAmountPayable(), wmp.getCardHolder(), wmp.getCardNumber(), wmp.getCVV(), wmp.getExpiryDate(), wmth.getTransactionHeaderId().toString(), sourceSystemReference, null);
				
				if (response.get("Success").equals("0"))
					throw new PaymentException("Unable to process payment: " + sourceSystemReference + " - " + wmth.getTransactionHeaderId().toString() + ": Response was  " + response.get("ResponseText") + " (" + response.get("ReCo") + ") " + response.get("HelpText") ); 
	
				receiptNumber = response.get("DpsTxnRef");
	
				if (webReceiptingSystem.equals(SourceSystem.RECEPTOR)) {
					receiptNumber = paymentTransactionMgr.receiptOnlinePayment(SourceSystem.MEMBERSHIP, transGroup.getAmountPayable(), wmp.getCardNumber(), wmth.getCreateDate(), wmp.getExpiryDate(), seqNumber, clientNumber.toString(), USER_MEMBERSHIP_WEB, SALES_BRANCH_WEB, LOCATION_WEB, TILL_WEB, wmp.getCardType(), wmp.getCardHolder(), null);
					// NOTE: RECEPTOR UPDATES PENDING PURCHASES AND LINE ITEMS ON RECEIPT. THIS INCLUDES WHO CREATED THE ORIGINAL
					// PAYABLE.
					
					LogUtil.log(this.getClass(), "receiptNumber=" + receiptNumber);
					// changes to web membership transaction and header will also commit at
					// the end of the tx
				}
			} else {
				receiptNumber = "0000000000000000";
			}

		} else if (webPaymentSystem.equals(SourceSystem.OCV)) {
			receiptNumber = payByOCV(wmth, wmp, transGroup, sourceSystemReference, clientNumber, seqNumber);
		} else {
			throw new Exception("No web payment provider found: " + getWebPaymentSystem());
		}
		return receiptNumber;
	}

	private void processDirectDebit(WebMembershipTransaction wmt, WebMembershipPayment wmp, TransactionGroup transGroup, MembershipVO newMemVO) throws SystemException, RemoteException, ValidationException {
		//Prepare a direct debit payment.
		
		// Create the account that will be used in the direct debit
		BankAccount bankAccount = createBankAccountFromWMP(wmp);
		
		// The frequency can be monthly or yearly (as a string) and is 
		// based on the termLength from b2c
		DirectDebitFrequency frequency = extractFrequencyFromWMT(wmt);

		// Create a calendar that is set to todays date. 
		DateTime today = new DateTime();
		Calendar cal = Calendar.getInstance();
	    cal.setTime(today);
	    
	    // Extract the day of the month from the calendar that will inform the direc debit subsystem 
	    // which day to add all the direct debits should be scheduled. 
	    int dayToDebit = cal.get(Calendar.DAY_OF_MONTH);
	    
	    // ddrDate is the date at which to actually extract the monies from the account.
	    // it is set here to be todays date, as we want to perform this immediately (in progress)
	    DateTime ddrDate = validateDDRDate(DateUtil.nextDay().formatShortDate());
	    
	    // Create the authority to perform the direct debit and add it to the transaction.  
		DirectDebitAuthority dda = new DirectDebitAuthority(null, newMemVO.getClientNumber(), transGroup.getUserID(), SALES_BRANCH_WEB, today, true, dayToDebit, false, ddrDate, frequency, bankAccount, SourceSystem.MEMBERSHIP, null);
		transGroup.setDirectDebitAuthority(dda, true);
	}

	private DirectDebitFrequency extractFrequencyFromWMT(WebMembershipTransaction wmt) throws SystemException {
		DirectDebitFrequency frequency;
		if(wmt.getMembershipTerm().equals("1:0:0:0:0:0:0")) {
			frequency = DirectDebitFrequency.getFrequency("Annually");
		} else if (wmt.getMembershipTerm().equals("0:12:0:0:0:0:0")) {
			frequency = DirectDebitFrequency.getFrequency("Monthly");
		} else {
			frequency = DirectDebitFrequency.getFrequency("Annually");
		}
		return frequency;
	}

	private BankAccount createBankAccountFromWMP(WebMembershipPayment wmp) throws SystemException, RemoteException, ValidationException {
		BankAccount bankAccount;
		
		if(wmp.getBsb() == null) {
			boolean validate = false;
			String paymentTypeStr = null;
			
			if(wmp.getCardType().equals("VISA")) {
				paymentTypeStr = PaymentTypeVO.PAYMENTTYPE_VISA;
			} else if (wmp.getCardType().equals("MASTERCARD")) {
				paymentTypeStr = PaymentTypeVO.PAYMENTTYPE_MASTER_CARD;
			} else {
				throw new SystemException("Failed to get payment type string '" + paymentTypeStr + "'.");
			}
			
			PaymentMgr paymentMgr = PaymentEJBHelper.getPaymentMgr();
			PaymentTypeVO paymentType = paymentMgr.getPaymentType(paymentTypeStr);
			if (paymentType == null) {
				throw new SystemException("Failed to get payment type '" + paymentTypeStr + "'.");
			}
			
			String exp = wmp.getExpiryDate();
			String expiry;
			if(exp.length() == 4) {
				expiry = exp.substring(0, 2) + "/" + exp.substring(2,4); 
				System.out.println(exp);
				System.out.println(expiry);
			} else {
				throw new ValidationException("Expiry must contain 4 alpha characters");
			}
			
			CreditCardAccount ccAccount = new CreditCardAccount();
			ccAccount.setValidate(validate);
			ccAccount.setAccountName(wmp.getCardHolder());
			ccAccount.setPaymentType(paymentType);
			ccAccount.setCardNumber(wmp.getCardNumber());
			ccAccount.setCardExpiry(expiry);
			ccAccount.setVerificationValue(wmp.getCVV());
			
			bankAccount = ccAccount;
		} else {
			bankAccount = new DebitAccount(wmp.getBsb(), wmp.getAccountNumber(), wmp.getAccountName());
		}
		
		return bankAccount;
	}
	
	private static DateTime validateDDRDate(String ddrDateStr) throws ValidationException {
		DateTime ddrDate = null;
		DateTime today = new DateTime();

		try {
			ddrDate = DateUtil.convertStringToDateTime(ddrDateStr);
		} catch (Exception e) {
			throw new ValidationException("The DDR date is not a valid date.");
		}

		return ddrDate;
	}

	/**
	 * 
	 * @param wmth
	 * @param wmp
	 * @param transGroup
	 * @param sourceSystemReference
	 * @param clientNumber
	 * @param seqNumber
	 * @return
	 * @throws Exception
	 */
	private String payByOCV(WebMembershipTransactionHeader wmth, WebMembershipPayment wmp, TransactionGroup transGroup, String sourceSystemReference, Integer clientNumber, String seqNumber) throws Exception {
		Collection<OCVTransaction> ocvTransactions = ocvTransactionMgr.getOCVTransactionsBySSRef(sourceSystemReference);
		for (Iterator<OCVTransaction> i = ocvTransactions.iterator(); i.hasNext();) {
			OCVTransaction tmp = i.next();
			if (tmp != null) {
				if (tmp.getReceiptReference() != null) {
					throw new GenericException("OCV Transaction has already been receipted (" + tmp.getReceiptReference() + ").");
				}
				if (OCVHelper.isOCVApproved(tmp.getResponseCode())) {
					throw new GenericException("OCV Transaction was processed on " + tmp.getCreateDate().formatMediumDate() + " and has already been approved.");
				}
			}
		}

		// create ocv transaction - new transaction
		OCVTransactionRequest transactionRequest = new OCVTransactionRequest();
		transactionRequest.setTransactionType(OCVTransactionRequest.TRANSACTION_TYPE_PURCHASE);
		transactionRequest.setCreateDate(new DateTime());
		transactionRequest.setUserId(transGroup.getUserID());
		transactionRequest.setClientId(USER_MEMBERSHIP_WEB);
		transactionRequest.setTransactionReference(sourceSystemReference);
		transactionRequest.setSourceSystem(SourceSystem.MEMBERSHIP.getAbbreviation());
		transactionRequest.setSourceSystemReference(sourceSystemReference);
		String accountNumber = FileUtil.getProperty("master", OCV_MERCHANT_ACCOUNT); // 4 //constants
		transactionRequest.setAccountNumber(accountNumber); // Predefined
															// Merchant!!!
		transactionRequest.setCardData(wmp.getCardNumber());
		transactionRequest.setCardExpiryDate(wmp.getExpiryDate());
		transactionRequest.setTotalAmount(new BigDecimal(NumberUtil.formatValue(transGroup.getAmountPayable())));

		String clientType = FileUtil.getProperty("master", OCV_CLIENT_TYPE); // constants
		transactionRequest.setClientType(clientType); // Internet payment
		LogUtil.log(this.getClass(), "transactionRequest" + transactionRequest);
		OCVTransactionResponse transactionResponse = ocvTransactionMgr.createPurchase(transactionRequest);
		LogUtil.log(this.getClass(), "transactionResponse=" + transactionResponse);

		// commit membership, transactions and pending transaction in receptor
		// receipt is committed as soon as the statement is completed - it is not
		// transaction aware.
		// OCV transaction will commit at the end - OCV transaction will have been
		// processed by the bank - new transaction
		transactionMgrLocal.commit();

		// create receptor receipt - complete receptor pending - this commits the
		// payment
		String receiptNumber = "";
		if (this.getWebReceiptingSystem().equals(SourceSystem.RECEPTOR)) {
			receiptNumber = paymentTransactionMgr.receiptOnlinePayment(SourceSystem.MEMBERSHIP, transGroup.getAmountPayable(), wmp.getCardNumber(), wmth.getCreateDate(), wmp.getExpiryDate(), seqNumber, clientNumber.toString(), USER_MEMBERSHIP_WEB, SALES_BRANCH_WEB, LOCATION_WEB, TILL_WEB, wmp.getCardType(), wmp.getCardHolder(), null);

			// NOTE: RECEPTOR UPDATES PENDING PURCHASES AND LINE ITEMS ON RECEIPT. THIS INCLUDES WHO CREATED THE ORIGINAL
			// PAYABLE.

			LogUtil.log(this.getClass(), "receiptNumber=" + receiptNumber);
			// changes to web membership transaction and header will also commit at
			// the end of the tx
		}

		// save receipt number against ocv transaction
		OCVTransaction ocvTransaction = ocvTransactionMgr.getOCVTransaction(transactionResponse.getTransactionId());
		ocvTransaction.setReceiptReference(receiptNumber);
		ocvTransactionMgr.updateOCVTransaction(ocvTransaction);

		return receiptNumber;
	}

}