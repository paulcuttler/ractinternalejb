package com.ract.web.convert.insurance;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import com.progress.open4gl.BigDecimalHolder;
import com.progress.open4gl.IntHolder;
import com.progress.open4gl.StringHolder;
import com.ract.common.SystemException;
import com.ract.util.DateTime;
import com.ract.util.LogUtil;
import com.ract.web.convert.common.ConvertProxyFactory;
import com.ract.web.convert.common.ConvertProxyPool;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.InRiskSi;
import com.ract.web.insurance.InRiskSiLineItem;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsClientDetail;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

public class ProgressConversionAdapter 
{
	private ConvertProxy pxy=null;	
	public ConversionResult convertPolicy(WebInsQuote quote,
			                              ArrayList<WebInsQuoteDetail> detList,
			                              ArrayList<WebInsClient> clientList,
			                              ArrayList<WebInsClientDetail> clientDetailList,
			                              ArrayList<InRiskSi> siList,
			                              String userId)throws Exception
	{
        ConversionResult res = null;
        try
        {
        	pxy = this.getProxy();
        	QuoteDetailResultSet detHolder = new QuoteDetailResultSet(detList);
        	WebClientResultSet cltHolder = new WebClientResultSet(clientList);
        	ClientDetResultSet cltDetHolder = new ClientDetResultSet(clientDetailList);
        	StringHolder resultHolder = new StringHolder();
        	IntHolder pNoHolder = new IntHolder();
        	GregorianCalendar gc = new GregorianCalendar();
        	gc.setTime(quote.getStartCover());
			SiHolder siHolder = new SiHolder(siList);
			SiLineHolder siLineHolder = new SiLineHolder(siList);
			IntHolder cNoHolder = new IntHolder();
			IntHolder sNoHolder = new IntHolder();
			BigDecimalHolder ph = new BigDecimalHolder();
			StringHolder messageHolder = new StringHolder();
			GregorianCalendar quoteDateGc = new GregorianCalendar();
			quoteDateGc.setTime(quote.getQuoteDate());

         	pxy.convertWebPolicy(quote.getWebQuoteNo().intValue(), 
        			             quote.getCoverType(), 
        			             quote.getQuoteStatus(), 
        			             quoteDateGc,
        			             gc, 
        			             detHolder,  
        			             cltHolder, 
        			             cltDetHolder,
                                 siHolder,
                                 siLineHolder,
        			             userId,
        			             resultHolder, 
        			             pNoHolder,
        			             cNoHolder,
        			             sNoHolder,
        			             ph,
        			             messageHolder);
        	res = new ConversionResult();
        	res.resultText = resultHolder.getStringValue();
/*System.out.println("\n-------------------------------------------"
		         + "\nConversion completed "
		         + "\n" + res.resultText
		         + "\n" + res.policyNo); */       	
        	if(pNoHolder!=null
        			&& !pNoHolder.isNull())res.policyNo = new Integer(pNoHolder.getIntValue());
        	if(!cNoHolder.isNull()) res.clientNo = new Integer(cNoHolder.getIntValue());
        	if(!sNoHolder.isNull()) res.seqNo = new Integer(sNoHolder.getIntValue());
        	if(!ph.isNull()) res.payableAmount = ph.getBigDecimalValue();
        	res.messages = messageHolder.getStringValue();

        }
        catch(Exception ex)
        {
        	throw ex;
        }
        finally
        {
        	this.releaseProxy(pxy);
        }
        return res;
 	}
	
	public ConversionResult convertQuote(WebInsQuote quote,
			                             String riskType,
			                             ArrayList<WebInsQuoteDetail> detList,
			                             ArrayList<InRiskSi> siList,
			                             ArrayList<WebInsClient> clientList,
			                             ArrayList<WebInsClientDetail> clientDetailList,
			                             String userId)throws Exception
	{
		ConversionResult res = null;
		try
		{			
			pxy = this.getProxy();
			QuoteDetailResultSet detHolder = new QuoteDetailResultSet(detList);
			WebClientResultSet cltHolder = new WebClientResultSet(clientList);
			ClientDetResultSet cltDetHolder = new ClientDetResultSet(clientDetailList);
			StringHolder resultHolder = new StringHolder();
			IntHolder qNoHolder = new IntHolder();
			GregorianCalendar gcStartDate = new GregorianCalendar();
			SiHolder siHolder = new SiHolder(siList);
			SiLineHolder siLineHolder = new SiLineHolder(siList);

			if(quote.getQuoteDate()!=null)
			{
			   gcStartDate.setTime(quote.getQuoteDate());
			}
			GregorianCalendar gcCreateDate = new GregorianCalendar();
			if(quote.getCreateDate()!=null)
			{
				gcCreateDate.setTime(quote.getCreateDate());
			}
			pxy.convertWebQuote(quote.getWebQuoteNo().intValue(), 
					            riskType, 
					            quote.getQuoteStatus(),
					            gcCreateDate,
					            gcStartDate, 
					            detHolder,
                                siHolder,
                                siLineHolder,
					            cltHolder, 
					            cltDetHolder,
					            userId,
					            resultHolder, 
					            qNoHolder);
			res = new ConversionResult();
			res.resultText = resultHolder.getStringValue();
			if(qNoHolder!=null
					&& !qNoHolder.isNull())res.quoteNo = new Integer(qNoHolder.getIntValue());
		}
		catch(Exception ex)
		{
			System.out.println("\nProgressConversionAdapter.convertQuote: Error sending to progress");
			ex.printStackTrace();
			throw ex;
		}
		finally
		{
			this.releaseProxy(pxy);
		}
		return res;
			}
	

	
	public String getWebBranchList()
	{
		String branchList = null;
		try
		{
		   StringHolder brHolder = new StringHolder();
		   pxy = this.getProxy();
		   pxy.getWebInsBranches(brHolder);
		   branchList = brHolder.getStringValue();
		}
		catch(Exception ex)
		{
			System.out.println("Unable to get sales branch list");
			ex.printStackTrace();
		}
		finally
		{
			this.releaseProxy(pxy);
		}

		return branchList;
	}
	
	
	public String getRestriction(String agentCode)
	{
		String restriction = "";
		try
		{
			StringHolder nameHolder = new StringHolder();
			StringHolder restrictionHolder  = new StringHolder();
			pxy = this.getProxy();
			pxy.getAgent(agentCode,
					     nameHolder,
					     restrictionHolder);
			restriction = restrictionHolder.getStringValue();
		}
		catch(Exception ex)
		{
			System.out.println("Error getting agent restriction");
		}
		finally
		{
			this.releaseProxy(pxy);
		}
		return restriction;
	}
	
	public BigDecimal getEligibleDiscount(Integer ractClientNo,
			                              String prodType,
			                              String riskType,
			                              DateTime coverDate)
	{
		BigDecimal discount = null;
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(coverDate);
		try
		{
			BigDecimalHolder dHolder = new BigDecimalHolder();
			pxy = this.getProxy();
			pxy.getEligibleDiscount(ractClientNo.intValue(), 
					                prodType, 
					                riskType, 
					                gc, 
					                dHolder);
			discount = dHolder.getBigDecimalValue();
		}
		catch(Exception ex)
		{
			System.out.println("Unable to get eligible discount in ProgressConversionAdapter.getEligibleDiscount" 
					    + "\n for Ract Client No " + ractClientNo
					    + "\n          prod type " + prodType
					    + "\n          risk Type " + riskType
					    + "\n         cover date " + coverDate
					    + "\n " + ex);
			
		}
		finally
		{
			this.releaseProxy(pxy);
		}
		return discount;
	}

	private ConvertProxy getProxy()throws SystemException
	{
		if(this.pxy==null)
		{
			try
			{
				pxy = (ConvertProxy)ConvertProxyPool.getInstance().borrowObject(ConvertProxyFactory.KEY_WEB_CONV_PROXY);
			}
			catch(Exception ex)
			{
				throw new SystemException(ex);
			}
		}
		return pxy;
	}

	private void releaseProxy(ConvertProxy proxy)
	{
		try
		{
			ConvertProxyPool.getInstance().returnObject(ConvertProxyFactory.KEY_WEB_CONV_PROXY,proxy);
		}
		catch(Exception ex)
		{
			LogUtil.fatal(this.getClass(), "Error releasing insurance progress proxy. Exception:" + ex.getMessage());
		}
		finally
		{
			this.pxy = null;
			//setting the company name to null causes some issues as release must be called multiple times
//			this.companyName = null;
		}
	}

	private class QuoteDetailResultSet extends com.ract.web.insurance.ProgressResultSet
	{
		/* dummy methods are implemented in ProgressResultSet
		 * */
		ArrayList<WebInsQuoteDetail> dataList = null;
		int position;
		WebInsQuoteDetail vd = null;
		public QuoteDetailResultSet(ArrayList<WebInsQuoteDetail> vl)
		{
			this.dataList = vl;
			this.position = -1;	
			if(dataList == null) dataList = new ArrayList<WebInsQuoteDetail>();
		}

		public Object getObject(int f)
		{
			Object ob = null;
			switch(f)
			{
			case 1:
				ob = vd.getFieldName();
				break;
			case 2:
				ob = vd.getFieldValue();
				break;
			case 3:
				ob = vd.getStringKey();
				break;
			case 4:
				ob = vd.getInKey();
			}
			return ob;
		}

		public boolean next()
		{
			position++;
			if(position >= dataList.size())return false;
			else{
				vd = this.dataList.get(position);
				return true;
			}
		}
	}

	private class SiHolder extends com.ract.web.insurance.ProgressResultSet
	{

		ArrayList<InRiskSi> siList = null;
		int position = 0;
		InRiskSi si = null;
		
		public SiHolder(ArrayList<InRiskSi> list)
		{
			siList = list;
			position = -1;
			if(siList == null) siList = new ArrayList<InRiskSi>();
		}

		public Object getObject(int f) throws SQLException {
			Object ob = null;
			switch(f)
			{
				case 1:
					ob = si.getSiSeqNo();
					break;
				case 2:
					ob = si.getSiClass();
					break;
					
			}
			return ob;
		}

		public boolean next() throws SQLException {
			position++;
			if(position >= siList.size())return false;
			else 
			{
				si = siList.get(position);
				return true;
			}
		}
	}
	
	private class SiLineHolder extends com.ract.web.insurance.ProgressResultSet
	{
		ArrayList<InRiskSiLineItem> liList = null;
		InRiskSiLineItem item = null;
		int position = -1;
		
		public SiLineHolder(ArrayList<InRiskSi> siList )
		{
System.out.println("xxxxx instantiate SiLineHolder xxxx");			
			position = -1;
			InRiskSi si = null;
			InRiskSiLineItem li = null;
			liList = new ArrayList<InRiskSiLineItem>();
			if(siList != null)
			{
				for(int xx = 0; xx< siList.size(); xx++)
				{
					si = siList.get(xx);
					for(int yy = 0; yy<si.getLineCount();yy++)
					{
						li = si.getLineItem(yy);
						liList.add(li);
					}
				}
			}
System.out.println("SiLineHolder instantiated with " + liList.size() + " lines");			
		}
		public boolean next() throws SQLException
		{
			position++;
			if(position >= liList.size()) return false;
			else
			{
				item = liList.get(position);
				return true;
			}
		}
		public Object getObject(int f) throws SQLException 
		{
			Object obj = null;
			Integer si = null;
			switch(f)
			{
			   case 1:
				   obj = item.getSiSeqNo();
				   break;
			   case 2:
				   obj = item.getSiLineNo();
				   break;
			   case 3:
				   obj = item.getDescription();
				   break;
			   case 4:
				   obj = item.getSerialNo();
				   break;
			   case 5:
				   if(item.getSumIns()!=null) si = item.getSumIns().intValue();
				   obj = si;
			    break;	   
			}
			return obj;
		}
	}
	
	private class WebClientResultSet extends com.ract.web.insurance.ProgressResultSet
	{
		// dummy methods are implemented in ProgressResultSet
		ArrayList<WebInsClient> dataList = null;
		int position;
		WebInsClient vd = null;
		
		public WebClientResultSet(ArrayList<WebInsClient> vl)
		{
			this.dataList = vl;
			this.position = -1;
			if(dataList == null) dataList = new ArrayList<WebInsClient>();
		}
		public Object getObject(int f)
		{
			Object ob = null;
			switch(f)
			{
			case 1:       
				ob = vd.getWebClientNo();
				break;
			case 2:
				ob = vd.getRactClientNo();
				break;
			case 3:
				ob = new Boolean(vd.isDriver());
				break;
			case 4:
				ob = new Boolean(vd.isOwner());
				break;
			case 5:
				ob = new Boolean(vd.isCltGroup());
				break;
			case 6:
				ob = new Boolean(vd.isPreferredAddress());
				break;
			case 7:
				ob = vd.getDrivingFrequency();
				break;
			case 8:
				ob = vd.getYearCommencedDriving();
				break;
			case 9:
				ob = vd.getNumberOfAccidents();
				break;
			case 10:
				ob = vd.getCurrentNCD();
				break;
			case 11:
				ob = vd.getGivenNames();
				break;
			case 12:
				ob = vd.getSurname();
				break;
			case 13:
				ob = vd.getGender();
				break;
			case 14:
				if(vd.getBirthDate()!=null)
				{
					GregorianCalendar gc = new GregorianCalendar();
					gc.setTime(vd.getBirthDate());
					ob = gc;
				}
				else ob = null;
				break;
			}
			return ob;
		}

		public boolean next()
		{
			position++;
			if(position >= dataList.size())return false;
			else{
				vd = this.dataList.get(position);
				return true;
			}
		}
	}

	private class ClientDetResultSet extends com.ract.web.insurance.ProgressResultSet
	{
	   ArrayList<WebInsClientDetail> dataList = null;
	   int position;
	   WebInsClientDetail vd = null;
		
	   public ClientDetResultSet(ArrayList<WebInsClientDetail> list)
       {
    	  this.dataList = list;
    	  this.position = -1;
    	  if(dataList == null) dataList = new ArrayList<WebInsClientDetail>();
       }
		
		public boolean next()
		{
			position++;
			if(position >= dataList.size())return false;
			else{
				vd = this.dataList.get(position);
				return true;
			}
		}
		public Object getObject(int f)
		{
			Object ob = null;
			switch(f)
			{
			case 1:  
				ob = vd.getWebClientNo();
				break;
			case 2:
				ob = vd.getDetailType();
				break;
			case 3:
				ob = vd.getDetMonth();
				break;
			case 4:
				ob = vd.getDetYear();
				break;
			case 5:
				ob = vd.getDetail();
				break;				
			}
			return ob;
		}		
	}
	
    public InRfDet getLookupDescription(String productType, String riskType, String fieldName, String fieldValue) throws Exception {
    	com.ract.web.insurance.InsAdapter adapter = new com.ract.web.insurance.InsAdapter();
    	
    	List<InRfDet> lookupData = adapter.getLookupData(productType, riskType, fieldName, new DateTime());
    	
    	for(InRfDet detail : lookupData) {
    	    if(detail.getrClass().equalsIgnoreCase(fieldValue)) {
    	        return detail;
    	    }
    	}
    	
    	LogUtil.warn(getClass(), "Returning empty InRfDet...");
    	InRfDet det = new InRfDet();
    	det.setrDescription(fieldValue);
    	return det;
    }
}
