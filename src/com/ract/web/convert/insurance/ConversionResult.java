package com.ract.web.convert.insurance;

import java.io.Serializable;
import java.math.BigDecimal;

public class ConversionResult implements Serializable 
{
    private static final long serialVersionUID = 4274037475685283460L;
   public String resultText;
   public Integer policyNo;
   public Integer quoteNo;
   public Integer clientNo;
   public Integer seqNo;
   public BigDecimal payableAmount;
   public String messages; 
   
   public ConversionResult(){}
   public ConversionResult(String rt,
		                   Integer polNo)
   {
	   this.resultText = rt;
	   this.policyNo = polNo;
   }
}
