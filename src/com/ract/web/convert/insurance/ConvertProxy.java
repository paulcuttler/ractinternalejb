package com.ract.web.convert.insurance;

import com.ract.common.CommonConstants;
import com.ract.common.proxy.ProgressProxy;
import com.ract.web.convert.ins.WebInsConvertProxy;

public class ConvertProxy extends WebInsConvertProxy implements ProgressProxy
{
	public ConvertProxy()throws Exception
	{
		   super(CommonConstants.getProgressAppServerURL(),  
				 CommonConstants.getProgressAppserverUser(),
			     CommonConstants.getProgressAppserverPassword(),
			     null);
	}
}
