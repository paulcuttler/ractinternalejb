package com.ract.web.convert.insurance;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.ract.common.GenericException;
import com.ract.common.SourceSystem;
import com.ract.membership.TransactionMgrLocal;
import com.ract.payment.PaymentTransactionMgrLocal;
import com.ract.payment.ocv.OCVHelper;
import com.ract.payment.ocv.OCVTransaction;
import com.ract.payment.ocv.OCVTransactionMgrLocal;
import com.ract.payment.ocv.OCVTransactionRequest;
import com.ract.payment.ocv.OCVTransactionResponse;
import com.ract.util.DateTime;
import com.ract.util.FileUtil;
import com.ract.web.convert.common.BaseConversionManager;
import com.ract.web.insurance.InRfDet;
import com.ract.web.insurance.InRiskSi;
import com.ract.web.insurance.WebInsClient;
import com.ract.web.insurance.WebInsClientDetail;
import com.ract.web.insurance.WebInsMgrLocal;
import com.ract.web.insurance.WebInsQuote;
import com.ract.web.insurance.WebInsQuoteDetail;

@Stateless
@Remote({ConvertPolicyMgrRemote.class})
@Local({ConvertPolicyMgrLocal.class})
public class ConvertPolicyMgr extends BaseConversionManager implements ConvertPolicyMgrRemote 
{
	@PersistenceContext
	EntityManager em;
	
	@EJB
	private OCVTransactionMgrLocal ocvTransactionMgr;
	@EJB
	private TransactionMgrLocal transactionMgrLocal;
	@EJB
	private PaymentTransactionMgrLocal paymentTransactionMgr;
																					// different till
	

  private ProgressConversionAdapter pa = null;	
	@EJB
  private WebInsMgrLocal bean;
	
  private ProgressConversionAdapter getAdapter()
  {
	  if(pa == null) pa = new ProgressConversionAdapter();
	  return pa;
  }
	
  public String convertPolicy(Integer quoteNo,
		                       String userId)throws Exception
  {
//	  this.connect(); 
	  WebInsQuote quote = bean.getQuote(quoteNo);
	  String det = bean.getQuoteDetail(quoteNo,"securedQuoteNo");
	  if(det==null || det.equals(""))
	  {
		  bean.setQuoteDetail(quoteNo, "securedQuoteNo",quote.getSecuredQuoteNo());
		  
	  }
	  ArrayList<WebInsQuoteDetail> quoteDetList = bean.getAllQuoteDetails(quoteNo);
	  ArrayList<WebInsClient> clientList = bean.getClients(quoteNo);
	  ArrayList<WebInsClientDetail> clientDetList = new ArrayList<WebInsClientDetail>();
    ArrayList<InRiskSi> siList = bean.getSpecifiedItems(quoteNo);

    if(clientList!=null)
    {
		  for(int xx=0;xx<clientList.size();xx++)
		  {
			  WebInsClient clt = clientList.get(xx);
			  ArrayList<WebInsClientDetail> aCltDetList = bean.getClientDetails(clt.getWebClientNo());
			  for(int yy = 0;yy<aCltDetList.size();yy++)
			  {
				  clientDetList.add(aCltDetList.get(yy));
			  }
		  }
    }
    else
    {
  	  return "Failure: no attached clients";
    }
      
	  ProgressConversionAdapter pa = getAdapter();
	  ConversionResult returnValue = pa.convertPolicy(quote, 
			                                          quoteDetList, 
			                                          clientList, 
			                                          clientDetList,
			                                          siList,
			                                          userId);
	  
	  if(returnValue.resultText.trim().equalsIgnoreCase("Success"))
	  {
		  returnValue.resultText += "\nPolicy Number " + returnValue.policyNo;
		  bean.setQuoteStatus(quoteNo, WebInsQuote.CONVERTED);
		  bean.setQuoteDetail(quoteNo,"policyNo", returnValue.policyNo.toString());
		  bean.setQuoteDetail(quoteNo,"clientNo", returnValue.clientNo.toString());
		  bean.setQuoteDetail(quoteNo,"OiSeqNo", returnValue.seqNo.toString());
	      String payMethod = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.PAY_METHOD);
	      String accountNo = bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NO);
	      
	      String receiptNumber = null;
				SourceSystem webPaymentSystem = getWebPaymentSystem();
				SourceSystem webReceiptingSystem = getWebReceiptingSystem();
							      
        if(payMethod.equals(WebInsQuoteDetail.PAY_ANNUALLY)
        && accountNo != null && !accountNo.equals("")) /* don't know if this is the correct version */
	      {
      	  try
      	  {		     
      	  	if (webPaymentSystem.equals(SourceSystem.OCV)) {
      	  		System.out.println("*** payment sys is OCV");
      	  		/* pay immediately by OCV */
     		     /* note, will use quoted amount.  The conversion program must ensure that 
     		      * the actual amount is exactly equal to the quoted amount,
     		      * particularly for immediate payments.
     		      */
      	  		receiptNumber = payByOCV(quoteNo,
                   userId,
                   returnValue);
      	  		
      	  	} else if (webPaymentSystem.equals(SourceSystem.DPS)) {
      	  		System.out.println("*** payment sys is DPS");
      	  		Map<String,String> response = payByDPS(transactionMgrLocal,
      	  				returnValue.payableAmount.doubleValue(), 
      	  				bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.DD_NAME),
      	  				bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.DD_NO),
      	  				null,
      	  				convertExpiryDate(bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.DD_EXP)),
      	  				quoteNo.toString(),
      	  				returnValue.clientNo + "W" + returnValue.seqNo, null);
      	  		
      	  		receiptNumber = response.get("DpsTxnRef");
      	  		
      	  		if (webReceiptingSystem.equals(SourceSystem.RECEPTOR)) {
      	  			System.out.println("*** rec sys is RECEPTOR");
      	  			try {
      		    		receiptNumber = paymentTransactionMgr.receiptOnlinePayment(
	      		    			SourceSystem.INSURANCE_RACT,
											returnValue.payableAmount.doubleValue(),
											bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NO),
											new DateTime(),
											convertExpiryDate(bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.DD_EXP)),
											returnValue.seqNo.toString(),
											returnValue.clientNo.toString(),
											USER_INS_WEB,
											SALES_BRANCH_WEB, 
											LOCATION_WEB, 
											TILL_WEB, 
											bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_TYPE).toUpperCase(),
											bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.DD_NAME).toUpperCase(),
											null);
      		    		
      		      } catch(Exception ep) {
      		    		throw new Exception("DPS payment completed,"
      		    				          + "\nbut unable to receipt payment"
      		    				          + "\n" + ep);
      		    	}
      	  		}
      	  	} else {
      				throw new Exception("No web payment provider found: " + getWebPaymentSystem());
      			}	
	  
      	    // store the final payable amount
      	  	bean.setQuoteDetail(quoteNo, "payableAmount", returnValue.payableAmount.toPlainString()); 
      	  	
      	  	// store the receipt reference
      	  	bean.setQuoteDetail(quoteNo, "receiptReference", receiptNumber);
      	  	
      	  	if (webReceiptingSystem.equals(SourceSystem.ECR)) {
	      	  	//generatePaymentTransactionFile();      	  	      	  	
	      	  	returnValue.resultText += "\nPlease allow a few minutes for a receipt to be created.";
      	  	}
      	  	
      	  }
      	  catch(Exception ex)
      	  {
      		  /* don't attempt to roll back the conversion process */
      		  ex.printStackTrace();
      		  returnValue.resultText += "\nPolicy created"
      			                     + "\nClient opted for immediate payment BUT"
      			                     + "\n" + ex.getMessage(); 
      	  }
	      }
        
        /* now that we have finished with the account number, obfuscate */
        if(accountNo != null && !accountNo.equals(""))
        {        	 
      	     accountNo = obfuscate(accountNo);
      	     bean.setQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NO,accountNo.toString());		  
        }
        
				if(!returnValue.messages.equals(""))
				{
				  returnValue.resultText += "\nPolicy created but\n" + returnValue.messages;
				}
	  } else {
  		System.out.println("*** conversion FAILED.");
	  }
	  return returnValue.resultText;
   }
  
  /**
   * Need to format expiry data as mmyy - no dashes, no spaces, 4 characters, leading zeros if required
   * @param input
   * @return
   */
  private String convertExpiryDate(String input) {
  	String expDate ;
    int month = 0;
    int year = 0;
    String[] exArray;
  	
    expDate = input;
		exArray = expDate.split("/");
		month = Integer.parseInt(exArray[0]) + 1000;
		year = Integer.parseInt(exArray[1]) + 1000;
		String t = "" + month;
		t = t.substring(2);
		expDate = t;
		t = "" + year;
		t = t.substring(2);
		expDate = expDate + t;
		
		return expDate;
  }
  
  public String payByOCV(Integer quoteNo,
		                String userId,
		                ConversionResult res)throws Exception
  {
	    
    String sourceSystemReference = res.clientNo + "W" + res.seqNo;
	    
		//prevent duplicate OCV transactions
		Collection<OCVTransaction> ocvTransactions = ocvTransactionMgr.getOCVTransactionsBySSRef(sourceSystemReference);
		for (Iterator<OCVTransaction> i = ocvTransactions.iterator();i.hasNext();)
		{
			OCVTransaction tmp = i.next();
			if (tmp != null)
			{
				if (tmp.getReceiptReference() != null)
				{
					throw new GenericException("OCV Transaction has already been receipted ("+tmp.getReceiptReference()+").");
				}
				if (OCVHelper.isOCVApproved(tmp.getResponseCode()))
				{
					throw new GenericException("OCV Transaction was processed on "+tmp.getCreateDate().formatMediumDate()+" and has already been approved.");					
				}
			}
		}
	    
		OCVTransactionRequest transactionRequest = new OCVTransactionRequest();
		transactionRequest.setTransactionType(OCVTransactionRequest.TRANSACTION_TYPE_PURCHASE);
		transactionRequest.setCreateDate(new DateTime());
		transactionRequest.setUserId(userId);
		transactionRequest.setClientId(USER_INS_WEB);
		transactionRequest.setTransactionReference(sourceSystemReference);   // ocv transaction reference no - think of a number
		transactionRequest.setSourceSystem("INS");
		transactionRequest.setSourceSystemReference(sourceSystemReference);
		String accountNumber = FileUtil.getProperty("master",OCV_MERCHANT_ACCOUNT); // 4 //constants
		transactionRequest.setAccountNumber(accountNumber); // Predefined
		transactionRequest.setTotalAmount(res.payableAmount);																										// Merchant!!!
		transactionRequest.setCardData(bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.DD_NO));

		String expDate = convertExpiryDate(bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.DD_EXP));
		
		transactionRequest.setCardExpiryDate(expDate);
		String clientType = FileUtil.getProperty("master", OCV_CLIENT_TYPE); // constants
		transactionRequest.setClientType(clientType); // Internet payment
//System.out.println("\n\n>>>------> OCV Request: \n" + transactionRequest.toRequestString());
    OCVTransactionResponse transactionResponse = null;
    try
    {
    transactionResponse = ocvTransactionMgr.createPurchase(transactionRequest);
    }
    catch(Exception ex)
    {
     throw new Exception("OCV payment not collected: \nerror: " + ex);	

    }
    if(!transactionResponse.isApproved())
    {
    	throw new Exception("OCV Payment not approved:"  + transactionResponse.getResponseText());
    }
//System.out.println("OCVRequest is complete: " + transactionResponse.getResponseText());		
		
		// commit membership, transactions and pending transaction in receptor
		// receipt is committed as soon as the statement is completed - it is not
		// transaction aware.
		// OCV transaction will commit at the end - OCV transaction will have been
		// processed by the bank - new transaction

		// create receptor receipt - complete receptor pending - this commits the
		// payment
				
    String receiptNumber = "";
    if (getWebReceiptingSystem().equals(SourceSystem.RECEPTOR)) {
			if(transactionResponse.isApproved()) {
	    	try
	    	{
	    		receiptNumber = paymentTransactionMgr.receiptOnlinePayment(SourceSystem.INSURANCE_RACT,
					                                                   res.payableAmount.doubleValue(),
					                                                   bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_NO),
					                                                   new DateTime(),
					                                                   expDate,
					                                                   res.seqNo.toString(), //What seq no??
					                                                   res.clientNo.toString(),
					                                                   USER_INS_WEB,
					                                                   SALES_BRANCH_WEB, 
					                                                   LOCATION_WEB, 
					                                                   TILL_WEB, 
					                                                   bean.getQuoteDetail(quoteNo, WebInsQuoteDetail.DD_TYPE).toUpperCase(),
					                                                   bean.getQuoteDetail(quoteNo,WebInsQuoteDetail.DD_NAME).toUpperCase(),
					                                                   null);
	      }
	    	catch(Exception ep)
	    	{
	    		throw new Exception("OCV payment completed,"
	    				          + "\nbut unable to receipt payment"
	    				          + "\n" + ep);
	    	}
	    }
    }
		//NOTE: RECEPTOR UPDATES PENDING PURCHASES AND LINE ITEMS ON RECEIPT. THIS INCLUDES WHO CREATED THE ORIGINAL
		// PAYABLE.
		
		// changes to web membership transaction and header will also commit at
		// the end of the tx

		// save receipt number against ocv transaction
		OCVTransaction ocvTransaction = ocvTransactionMgr.getOCVTransaction(transactionResponse.getTransactionId());
		ocvTransaction.setReceiptReference(receiptNumber);
		ocvTransactionMgr.updateOCVTransaction(ocvTransaction);

		return receiptNumber;
  }
   
   public String convertQuote(Integer quoteNo,
		                      String riskType,
                              String userId)throws Exception
   {
      String retString;
      WebInsQuote quote = bean.getQuote(quoteNo);
	  String det = bean.getQuoteDetail(quoteNo,"securedQuoteNo");
	  if(det==null || det.equals(""))
	  {
		  bean.setQuoteDetail(quoteNo, "securedQuoteNo",quote.getSecuredQuoteNo());
		  
	  }

      ArrayList<WebInsQuoteDetail> quoteDetList = bean.getAllQuoteDetails(quoteNo);
      ArrayList<WebInsClient> clientList = bean.getClients(quoteNo);
      if(clientList==null)clientList = new ArrayList<WebInsClient>();  // can't be null, can be empty list
      ArrayList<WebInsClientDetail> clientDetList = new ArrayList<WebInsClientDetail>();
      ArrayList<InRiskSi> siList = bean.getSpecifiedItems(quoteNo);
      if(clientList!=null)
      {
    	  for(int xx=0;xx<clientList.size();xx++)
    	  {
    		  WebInsClient clt = clientList.get(xx);
    		  ArrayList<WebInsClientDetail> aCltDetList = bean.getClientDetails(clt.getWebClientNo());
    		  for(int yy = 0;yy<aCltDetList.size();yy++)
    		  {
    			  clientDetList.add(aCltDetList.get(yy));
    		  }
    	  }
      }
      ProgressConversionAdapter pa = getAdapter();
      ConversionResult returnValue = pa.convertQuote(quote,
    		                                         riskType,
    		                                         quoteDetList,
    		                                         siList,
    		                                         clientList, 
    		                                         clientDetList,
    		                                         userId);
      if(returnValue.resultText.equalsIgnoreCase("Success"))
      {
    	  bean.setQuoteStatus(quoteNo, WebInsQuote.QUOTE_CONVERTED);
    	  bean.setQuoteDetail(quoteNo,"RactQuoteNo", returnValue.quoteNo.toString());
    	  retString = "Created quote number " + returnValue.quoteNo;
      }
      else retString = returnValue.resultText;
      return retString;
   }

   
//	private void connect()throws Exception
//	{
//		if(this.bean==null)
//		{
//			Properties properties = new Properties();
//			properties.put("java.naming.factory.initial",
//			"org.jnp.interfaces.NamingContextFactory");
//			properties.put("java.naming.factory.url.pkgs",
//			"=org.jboss.naming:org.jnp.interfaces");
//			String jnp = com.ract.util.FileUtil.getProperty("master","java.naming.provider.url");
//			properties.put("java.naming.provider.url", jnp);
//
//			InitialContext ctx = new InitialContext(properties);
//			bean = (WebInsMgrRemote)ctx.lookup("WebInsMgr/remote");
//		}
//	}
	
	
	public String getWebBranchList() throws Exception
	{
		ProgressConversionAdapter pa = getAdapter();
		return pa.getWebBranchList();
	}
	public BigDecimal getEligibleDiscount(Integer ractClientNo,
			                              String prodType,
			                              String riskType,
			                              DateTime coverDate)
	{
		ProgressConversionAdapter pa = getAdapter();
		return pa.getEligibleDiscount(ractClientNo,
				                      prodType, 
				                      riskType, 
				                      coverDate);
	}

    public InRfDet getLookupDescription(String productType, String riskType, String fieldName, String fieldValue) throws Exception {
        ProgressConversionAdapter pa = getAdapter();
        return pa.getLookupDescription(productType, riskType, fieldName, fieldValue);
    }
    
    public String getRestriction(String pCode )throws Exception
    {
    	if(pCode.equals("")) return "";
    	else 
    	{
    		ProgressConversionAdapter pa = getAdapter();
    		return pa.getRestriction(pCode);
    	}
    }

}
