package com.ract.web.convert.insurance;

import java.math.BigDecimal;

import javax.ejb.Remote;

import com.ract.util.DateTime;
import com.ract.web.insurance.InRfDet;

@Remote
public interface ConvertPolicyMgrRemote 
{
	public String convertPolicy(Integer quoteNo,
			                    String userId)throws Exception;
	public String convertQuote(Integer quoteNo,
			                   String riskType,
                               String userId)throws Exception;
	public String getWebBranchList() throws Exception;
	public BigDecimal getEligibleDiscount(Integer ractClientNo,
            String prodType,
            String riskType,
            DateTime coverDate)throws Exception;
    public InRfDet getLookupDescription(String productType, String riskType, String fieldName, String fieldValue) throws Exception;
    public String payByOCV(Integer quoteNo,
            String userId,
            ConversionResult res)throws Exception;
    public String getRestriction(String pCode)throws Exception;
    
    public String obfuscate(String accountNo) throws Exception;
}
