package com.ract.web.convert.common;

import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.paymentexpress.ws.PxPostSupport;
import com.paymentexpress.ws.TransactionDetailsPxPost;
import com.ract.common.CommonEJBHelper;
import com.ract.common.SourceSystem;
import com.ract.common.schedule.ScheduleMgr;
import com.ract.membership.TransactionMgr;
import com.ract.util.FileUtil;
import com.ract.util.LogUtil;
import com.ract.web.common.PaymentFileGeneratorJob;

abstract public class BaseConversionManager {
	
	final static public String WEB_PAYMENT_SYSTEM_KEY = "webPaymentSystem";
	final static public String WEB_RECEIPTING_SYSTEM_KEY = "webReceiptingSystem";
	final static public String DPS_USERNAME_KEY = "dpsUsername";
	final static public String DPS_PASSWORD_KEY = "dpsPassword";
	protected static final String OCV_CLIENT_TYPE = "OCV_CLIENT_TYPE";
	protected static final String OCV_MERCHANT_ACCOUNT = "OCV_MERCHANT_ACCOUNT";
	public static final String SALES_BRANCH_WEB = "WB";
	public static final String LOCATION_WEB = "WEB";
	protected static final String TILL_WEB = "WEB01";
	public static final String USER_INS_WEB = "IWEB";
	
	public SourceSystem getWebPaymentSystem() throws Exception {
		String webPaymentSystem = FileUtil.getProperty("master", WEB_PAYMENT_SYSTEM_KEY);		
		return SourceSystem.getSourceSystem(webPaymentSystem);
	}
	
	public SourceSystem getWebReceiptingSystem() throws Exception {
		String webReceiptingSystem = FileUtil.getProperty("master", WEB_RECEIPTING_SYSTEM_KEY);		
		return SourceSystem.getSourceSystem(webReceiptingSystem);
	}
	
	/**
	 * THIS VERSION DROPPED
	 * @param amountPayable
	 * @param cardHolder
	 * @param cardNumber
	 * @param cvv
	 * @param expiryDate
	 * @param merchRef
	 * @param srcRef
	 * @return
	 * @throws Exception
	 */
//	public String payByDPS(TransactionMgr transactionMgr, double amountPayable, String cardHolder, String cardNumber, String cvv, String expiryDate, String merchRef, String srcRef) throws Exception {
//
//		final String DPS_AUTH_TRUE = "1";
//		final String DPS_AUTH_FALSE = "0";
//		final String DPS_USERNAME = FileUtil.getProperty("master", DPS_USERNAME_KEY);
//		final String DPS_PASSWORD = FileUtil.getProperty("master", DPS_PASSWORD_KEY);
//		final String DPS_TYPE_PURCHASE = "Purchase";
//		
//		NumberFormat df = new DecimalFormat("0.00");
//		
//		TransactionDetails txnDetails = new TransactionDetails();
//		txnDetails.setAmount(df.format(amountPayable));
//		txnDetails.setCardHolderName(cardHolder);
//		txnDetails.setCardNumber(cardNumber);
//		txnDetails.setCvc2(cvv);
//		txnDetails.setDateExpiry(expiryDate);
//		txnDetails.setMerchantReference(merchRef);
//		txnDetails.setTxnRef(srcRef);
//		txnDetails.setTxnType(DPS_TYPE_PURCHASE);				
//		
//		PaymentExpressWS paymentExpress = new PaymentExpressWS();
//		TransactionResult2 txnResult = paymentExpress.getPaymentExpressWSSoap().submitTransaction(DPS_USERNAME, DPS_PASSWORD, txnDetails);
//		String authCode = txnResult.getAuthCode();
//		String authorised = txnResult.getAuthorized();
//		String responseText = txnResult.getResponseText();
//		String responseCode = txnResult.getReco();
//		
//		String receiptNumber = txnResult.getDpsTxnRef();
//		
//		// actual receipting will be done as part of a back office process
//		
//		if (authorised.equals(DPS_AUTH_TRUE)) {
//			// commit membership
//			transactionMgr.commit();
//		} else {
//			throw new Exception("Unable to process payment: " + responseText + " (" + responseCode + ") " + txnResult.getHelpText());
//		}
//		
//		return receiptNumber;
//	}
	
	/**
	 * New version of payByDPS POSTs the payment transaction to PxPost via a Mulesoft Proxy, thus avoiding TLS 1.0 issue.
	 * 
	 * @param amountPayable
	 * @param cardHolder
	 * @param cardNumber
	 * @param cvv
	 * @param expiryDate
	 * @param merchRef
	 * @param srcRef
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> payByDPS(TransactionMgr transactionMgr, double amountPayable, String cardHolder, String cardNumber, String cvv, String expiryDate, String merchRef, String srcRef, Map<String, String> testCreds) throws Exception {

		final String DPS_AUTH_TRUE = "1";
		final String DPS_TYPE_PURCHASE = "Purchase";

		NumberFormat df = new DecimalFormat("0.00");

		// Build transaction details container from the provided parameters
		TransactionDetailsPxPost txnDetails = new TransactionDetailsPxPost();
		txnDetails.setAmount(df.format(amountPayable));
		txnDetails.setCardHolderName(cardHolder);
		txnDetails.setCardNumber(cardNumber);
		txnDetails.setCvc2(cvv);
		txnDetails.setDateExpiry(expiryDate);
		txnDetails.setMerchantReference(merchRef);
		txnDetails.setTxnRef(srcRef);
		txnDetails.setTxnType(DPS_TYPE_PURCHASE);

		// Execute the POST and convert the response payload to an XML DOM document
		Document result = null;
		if (testCreds == null || testCreds.isEmpty()) {
			result = toXmlDocument(PxPostSupport.postHTTPRequest(txnDetails, null, null, null));
		} else {
			result = toXmlDocument(PxPostSupport.postHTTPRequest(txnDetails, testCreds.get("un"), testCreds.get("pw"), testCreds.get("url")));
		}
			

		// Build a map of the response
		Map<String, String> responseMap = new HashMap<String, String>();

		// Transaction level data
		NodeList nList = result.getElementsByTagName("Transaction");

		// Get the authorisation code and the Success flag
		Node nNode = nList.item(0);
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			responseMap.put("AuthCode", eElement.getElementsByTagName("AuthCode").item(0).getTextContent());
			NamedNodeMap nNodeMap = nNode.getAttributes();
			responseMap.put("Success", nNodeMap.getNamedItem("success").getNodeValue());
		}

		// Txn level data
		nList = result.getElementsByTagName("Txn");

		// Get required response details
		nNode = nList.item(0);
		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
			Element eElement = (Element) nNode;
			responseMap.put("ResponseText", eElement.getElementsByTagName("ResponseText").item(0).getTextContent());
			responseMap.put("ReCo", eElement.getElementsByTagName("ReCo").item(0).getTextContent());
			responseMap.put("DpsTxnRef", eElement.getElementsByTagName("DpsTxnRef").item(0).getTextContent());
			responseMap.put("HelpText", eElement.getElementsByTagName("HelpText").item(0).getTextContent());
		}

		if (responseMap.get("Success").equals(DPS_AUTH_TRUE)) {
			// If the transaction succeeded, commit membership and log the transaction details
			if (transactionMgr != null)
				transactionMgr.commit();
			LogUtil.info(getClass(), "Payment transaction succeeded for " + txnDetails.getTxnRef() + " - " + txnDetails.getMerchantReference() + ". Response: " + responseMap.get("ResponseText") + ", Receipt number: " + responseMap.get("DpsTxnRef") + ", Authorisation code: " + responseMap.get("AuthCode"));
		} else {
			// If the transaction fails throw a Payment exception and log
			LogUtil.fatal(getClass(), "Unable to process payment: " + txnDetails.getTxnRef() + " - " + txnDetails.getMerchantReference() + ": Response was  " + responseMap.get("ResponseText") + " (" + responseMap.get("ReCo") + ") " + responseMap.get("HelpText"));
			//throw new PaymentException("Unable to process payment: " + txnDetails.getTxnRef() + " - " + txnDetails.getMerchantReference() + ": Response was  " + responseMap.get("ResponseText") + " (" + responseMap.get("ReCo") + ") " + responseMap.get("HelpText") ); 
		}

		return responseMap;
	}	
	
	/**
	 * Creates an XML Document from a String
	 * @param str
	 * @return the created document
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private static Document toXmlDocument(String str) throws ParserConfigurationException, SAXException, IOException {

		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document document = docBuilder.parse(new InputSource(new StringReader(str)));

		return document;
	}

	public void generatePaymentTransactionFile() {
		JobDetail jobDetail = new JobDetail("PaymentFileGeneratorJob",
        Scheduler.DEFAULT_GROUP,
        PaymentFileGeneratorJob.class);

		jobDetail.setDescription("Payment File Generator");

		SimpleTrigger trigger = new SimpleTrigger("PaymentFileGeneratorTrigger", Scheduler.DEFAULT_GROUP, new Date());
		
		try {
			ScheduleMgr scheduleMgr = CommonEJBHelper.getScheduleMgr();
			scheduleMgr.scheduleJob(jobDetail, trigger);
		
		} catch (Exception e) {
			LogUtil.fatal(getClass(), "Unable to schedule PaymentFileGeneratorJob: " + e);
		}
	}
	  
  /**
   * Obfuscate a bank account number
   * replace the middle 8 characters (if there are that many) with '*' characters
   * @param String Account number
   * @return String Obfuscated account number
   */
  @Deprecated
  public String obfuscate(String accountNo)
  {
	  String outString="";
	  int vLength;
	  String vFill;
	  char[] accArray = accountNo.toCharArray();
        
	  vLength = accountNo.length();
	  for(int xx = 4; xx < vLength - 4; xx++)
	  {
		  accArray[xx] = '*';
      }
	  outString = new String(accArray);	  
	  return outString;
  }
  
}
