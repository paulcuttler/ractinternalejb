package com.ract.web.convert.common;

import com.ract.util.DateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="[gn-stsub]")
public class GnStsub
{

  @Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((suburb == null) ? 0 : suburb.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GnStsub other = (GnStsub) obj;
		if (suburb == null)
		{
			if (other.suburb != null)
				return false;
		}
		else
			if (!suburb.equals(other.suburb))
				return false;
		return true;
	}

	@Id
  private Integer stsubid;
  private String suburb;
  private String street;
  private String postcode;

  @Column(name="[last-update]")
  private DateTime lastUpdate;
  private String country;
  private String state;

  @Column(name="[last-updateid]")
  private String lastUpdateId;

  public Integer getStsubid()
  {
    return this.stsubid;
  }

  public void setStsubid(Integer stsubid)
  {
    this.stsubid = stsubid;
  }

  public String getSuburb()
  {
    return this.suburb;
  }

  public void setSuburb(String suburb)
  {
    this.suburb = suburb;
  }

  public String getStreet()
  {
    return this.street;
  }

  public void setStreet(String street)
  {
    this.street = street;
  }

  public String getPostcode()
  {
    return this.postcode;
  }

  public void setPostcode(String postcode)
  {
    this.postcode = postcode;
  }

  public DateTime getLastUpdate()
  {
    return this.lastUpdate;
  }

  public void setLastUpdate(DateTime lastUpdate)
  {
    this.lastUpdate = lastUpdate;
  }

  public String getCountry()
  {
    return this.country;
  }

  public void setCountry(String country)
  {
    this.country = country;
  }

  public String getState()
  {
    return this.state;
  }

  public void setState(String state)
  {
    this.state = state;
  }

  public String getLastUpdateId()
  {
    return this.lastUpdateId;
  }

  public void setLastUpdateId(String lastUpdateId)
  {
    this.lastUpdateId = lastUpdateId;
  }


}