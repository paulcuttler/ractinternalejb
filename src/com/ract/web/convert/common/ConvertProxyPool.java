package com.ract.web.convert.common;

import org.apache.commons.pool.impl.*;

import com.ract.common.SystemParameterVO;
import com.ract.common.pool.ProxyObjectPool;

public 	class 	ConvertProxyPool 
		extends ProxyObjectPool
{

	private static GenericKeyedObjectPool pool;
	private static String proxyPoolKey = SystemParameterVO.CATEGORY_PROXY_POOL_CVT;
	
	
	private ConvertProxyPool()
	{
		
	}

	public synchronized static GenericKeyedObjectPool getInstance()
	{
		if (pool == null)
		{
	      	GenericKeyedObjectPool.Config conf =new ConvertProxyPool().getProxyPoolConfig();
	       	
	    	log("ConvertProxyPool ","creating pool " );
	    	pool = new GenericKeyedObjectPool	(	new ConvertProxyFactory(),
	    		  									conf
	    		  	                             );

		}
	     log("ConvertProxyPool ",toString(pool));
		return pool;
   }

   public static void removeCurrentObjectPool()
   {
     if (pool != null)
     {
       pool.clear();
       try
       {
         pool.close();
       }
       catch(Exception ex)
       {
         log("ConvertProxyObjectPool","Unable to close the object pool. "+ex.getMessage());
       }
       pool = null;
     }
   }

   @Override
   protected String getKey() 
   {
	   return proxyPoolKey;
   }

}
