package com.ract.web.convert.common;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

/**
 * Test suite for operation of payment express PxPost HTTPS POST service which replaces the SOAP service originally used
 * Suite uses the PxPost UAT gateway via a Mulesoft REST endpoint
 * @author sharpg-admin
 *
 */
public class TestPaymentExpress extends BaseConversionManager {

	Map<String, String> testCreds = new HashMap<String, String>();

	/**
	 * Provide gateway credentials and end point (usually kept in master.properties which ix not accessible in this context)
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		testCreds.clear();
		testCreds.put("un","RACTLtdDev0227");
		testCreds.put("pw","cdbc96ff");
		testCreds.put("url","http://mule-worker-internal-payment-express-test.au.cloudhub.io:8083/pxpost");
    }

    /**
     * Test a successful Visa transaction
     * @throws Exception
     */
	@Test
	public void testSuccessVisa() throws Exception {
		Map<String, String> response = payByDPS(null, new Double(15.00d), "Geoffrey A Sharp", "4111111111111111", "631", "0620", "Test Transaction", "inv1278", testCreds);
		printReturnValues(response);
		Assert.assertEquals(true, response.get("AuthCode").length() > 0 && response.get("AuthCode").matches(".*[0-9].*"));
		Assert.assertEquals(true, response.get("DpsTxnRef").length() > 0 && (response.get("DpsTxnRef").matches(".*[A-Za-z].*") && response.get("DpsTxnRef").matches(".*[0-9].*") && response.get("DpsTxnRef").matches("[A-Za-z0-9]*")));
		Assert.assertEquals(true, response.get("HelpText").length() > 0 && (response.get("HelpText").equals("Transaction Approved")));
		Assert.assertEquals(true, response.get("ReCo").equals("00"));
		Assert.assertEquals(true, response.get("ResponseText").equals("APPROVED"));
		Assert.assertEquals(true, response.get("Success").equals("1"));
	}

    /**
     * Test a successful Mastercard transaction
     * @throws Exception
     */
	@Test
	public void testSuccessMastercard() throws Exception {
		Map<String, String> response = payByDPS(null, new Double(15.00d), "Geoffrey A Sharp", "5431111111111111", "631", "0620", "Test Transaction", "inv1278", testCreds);
		printReturnValues(response);
		Assert.assertEquals(true, response.get("AuthCode").length() > 0 && response.get("AuthCode").matches(".*[0-9].*"));
		Assert.assertEquals(true, response.get("DpsTxnRef").length() > 0 && (response.get("DpsTxnRef").matches(".*[A-Za-z].*") && response.get("DpsTxnRef").matches(".*[0-9].*") && response.get("DpsTxnRef").matches("[A-Za-z0-9]*")));
		Assert.assertEquals(true, response.get("HelpText").length() > 0 && (response.get("HelpText").equals("Transaction Approved")));
		Assert.assertEquals(true, response.get("ReCo").equals("00"));
		Assert.assertEquals(true, response.get("ResponseText").equals("APPROVED"));
		Assert.assertEquals(true, response.get("Success").equals("1"));
	}

	/**
	 * Test a decline
	 * @throws Exception
	 */
	@Test
	public void testDecline() throws Exception {
		Map<String, String> response = payByDPS(null, new Double(15.00d), "Geoffrey A Sharp", "5431111111111301", "631", "0620", "Test Transaction", "inv1278", testCreds);
		printReturnValues(response);
		Assert.assertEquals(true, response.get("AuthCode").length() == 0);
		Assert.assertEquals(true, response.get("DpsTxnRef").length() > 0 && (response.get("DpsTxnRef").matches(".*[A-Za-z].*") && response.get("DpsTxnRef").matches(".*[0-9].*") && response.get("DpsTxnRef").matches("[A-Za-z0-9]*")));
		Assert.assertEquals(true, response.get("HelpText").length() > 0 && (response.get("HelpText").equals("The transaction was Declined (30)")));
		Assert.assertEquals(true, response.get("ReCo").equals("30"));
		Assert.assertEquals(true, response.get("ResponseText").equals("DECLINED"));
		Assert.assertEquals(true, response.get("Success").equals("0"));
	}

    /**
     * Test a decline due to a failure to transfer funds
     * @throws Exception
     */
	@Test
	public void testDeclineNotTransferred() throws Exception {
		Map<String, String> response = payByDPS(null, new Double(15.00d), "Geoffrey A Sharp", "5431111111111228", "631", "0620", "Test Transaction", "inv1278", testCreds);
		printReturnValues(response);
		Assert.assertEquals(true, response.get("AuthCode").length() == 0);
		Assert.assertEquals(true, response.get("DpsTxnRef").length() > 0 && (response.get("DpsTxnRef").matches(".*[A-Za-z].*") && response.get("DpsTxnRef").matches(".*[0-9].*") && response.get("DpsTxnRef").matches("[A-Za-z0-9]*")));
		Assert.assertEquals(true, response.get("HelpText").length() > 0 && (response.get("HelpText").equals("The transaction was declined.Funds were not transferred")));
		Assert.assertEquals(true, response.get("ReCo").equals("51"));
		Assert.assertEquals(true, response.get("ResponseText").equals("DECLINED"));
		Assert.assertEquals(true, response.get("Success").equals("0"));
	}

    /**
     * Test a decline for a card with a do not honour flag
     * @throws Exception
     */
	@Test
	public void testDeclineDontHonour() throws Exception {
		Map<String, String> response = payByDPS(null, new Double(15.00d), "Geoffrey A Sharp", "4999999999999236", "631", "0620", "Test Transaction", "inv1278", testCreds);
		printReturnValues(response);
		Assert.assertEquals(true, response.get("AuthCode").length() == 0);
		Assert.assertEquals(true, response.get("DpsTxnRef").length() > 0 && (response.get("DpsTxnRef").matches(".*[A-Za-z].*") && response.get("DpsTxnRef").matches(".*[0-9].*") && response.get("DpsTxnRef").matches("[A-Za-z0-9]*")));
		Assert.assertEquals(true, response.get("HelpText").length() > 0 && (response.get("HelpText").equals("The transaction was not approved")));
		Assert.assertEquals(true, response.get("ReCo").equals("05"));
		Assert.assertEquals(true, response.get("ResponseText").equals("DO NOT HONOUR"));
		Assert.assertEquals(true, response.get("Success").equals("0"));
	}

    /**
     * Test a decline for a card which fails the LUHN check
     * @throws Exception
     */
	@Test
	public void testDeclineFailedLuhnCheck() throws Exception {
		Map<String, String> response = payByDPS(null, new Double(15.00d), "Geoffrey A Sharp", "4111111111111119", "631", "0620", "Test Transaction", "inv1278", testCreds);
		printReturnValues(response);
		Assert.assertEquals(true, response.get("AuthCode").length() == 0);
		Assert.assertEquals(true, response.get("DpsTxnRef").length() > 0 && (response.get("DpsTxnRef").matches(".*[A-Za-z].*") && response.get("DpsTxnRef").matches(".*[0-9].*") && response.get("DpsTxnRef").matches("[A-Za-z0-9]*")));
		Assert.assertEquals(true, response.get("HelpText").length() > 0 && (response.get("HelpText").equals("The transaction was declined.Funds were not transferred")));
		Assert.assertEquals(true, response.get("ReCo").equals("76"));
		Assert.assertEquals(true, response.get("ResponseText").equals("DECLINED"));
		Assert.assertEquals(true, response.get("Success").equals("0"));
	}

    /**
     * Test an invalid transaction 
     * @throws Exception
     */
	@Test
	public void testInvalidTransaction() throws Exception {
		Map<String, String> response = payByDPS(null, new Double(15.00d), "Geoffrey A Sharp", "4999999999999269", "631", "0620", "Test Transaction", "inv1278", testCreds);
		printReturnValues(response);
		Assert.assertEquals(true, response.get("AuthCode").length() == 0);
		Assert.assertEquals(true, response.get("DpsTxnRef").length() > 0 && (response.get("DpsTxnRef").matches(".*[A-Za-z].*") && response.get("DpsTxnRef").matches(".*[0-9].*") && response.get("DpsTxnRef").matches("[A-Za-z0-9]*")));
		Assert.assertEquals(true, response.get("HelpText").length() > 0 && (response.get("HelpText").equals("The card used does not support this kind of transaction.")));
		Assert.assertEquals(true, response.get("ReCo").equals("12"));
		Assert.assertEquals(true, response.get("ResponseText").equals("INVALID TRANSACTION"));
		Assert.assertEquals(true, response.get("Success").equals("0"));
	}

    /**
     * Test a server error - in this case server error 500
     * @throws Exception
     */
	@Test
	public void testError500() throws Exception {
		try {
			payByDPS(null, new Double(15.00d), "Geoffrey A Sharp", "4999999999999202 ", "631", "0620", "Test Transaction", "inv1278", testCreds);
		} catch (Exception e) {
			Assert.assertTrue(e.getMessage().contains("result code is problematic (not 200)"));
		}
	}
	
	private void printReturnValues(Map<String,String> response) {
		System.out.println("AuthCode " + response.get("AuthCode"));
		System.out.println("Success " + response.get("Success"));
		System.out.println("ResponseText " + response.get("ResponseText"));
		System.out.println("ReCo " + response.get("ReCo"));
		System.out.println("DpsTxnRef " + response.get("DpsTxnRef"));
		System.out.println("HelpText " + response.get("HelpText"));
	}
}