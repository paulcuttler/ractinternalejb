package com.ract.web.convert.common;

import com.ract.common.pool.ProgressProxyObjectFactory;

public 	class 	ConvertProxyFactory 
		extends ProgressProxyObjectFactory 
{
	  public final static String KEY_WEB_CONV_PROXY = "WEB_CONV_PROXY";
	  
	  public Object makeObject(Object key) throws java.lang.Exception
	  {
	    if (key == null)
	    {
	      throw new Exception("Attempting to make an object with a null key.");
	    }

	    getPoolInfo(ConvertProxyPool.getInstance(), key);

	    if (key.toString().equals(KEY_WEB_CONV_PROXY))
	    {
	      return new com.ract.web.convert.insurance.ConvertProxy();
	    }
	    else
	    {
	    	//can make any other type
	    	return super.makeObject(key);
	    }
	  }

}
